<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeskMacTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('desk_mac', function(Blueprint $table)
		{
			$table->string('desk_mac_id')->references('products_id')->on('products');
			$table->string('desk_mac_model_name');
			$table->string('desk_mac_ram');
			$table->string('desk_mac_cpu');
			$table->string('desk_mac_hdd');
			$table->string('desk_mac_screen');
			$table->string('desk_mac_path');
			$table->text('desk_mac_desc'); 
			$table->string('desk_mac_price');
			$table->string('desk_mac_color');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('desk_mac');
	}

}
