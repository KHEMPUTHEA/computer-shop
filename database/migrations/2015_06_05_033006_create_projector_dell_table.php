<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectorDellTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projector_dell', function(Blueprint $table)
		{
			$table->string('projector_dell_id')->references('products_id')->on('products');
			$table->string('projector_dell_model_name');
			$table->string('brightness');
			$table->string('lamp_life');
			$table->string('projector_dell_path');
			$table->string('price');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projector_dell');
	}

}
