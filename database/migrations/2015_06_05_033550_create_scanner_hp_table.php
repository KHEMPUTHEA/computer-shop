<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScannerHpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scanner_hp', function(Blueprint $table)
		{
			$table->string('scanner_hp_id')->references('products_id')->on('products');
			$table->string('scanner_hp_model_name');
			$table->string('function');
			$table->string('scan');
			$table->string('scan_color');
			$table->string('paper_size');
			$table->string('port');
			$table->string('scanner_hp_path');
			$table->string('price');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('scanner_hp');
	}

}
