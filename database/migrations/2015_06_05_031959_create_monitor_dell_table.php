<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitorDellTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('monitor_dell', function(Blueprint $table)
		{
			$table->string('monitor_dell_id')->references('products_id')->on('products');
			$table->string('monitor_dell_model_name');
			$table->string('display');
			$table->string('resolution');
			$table->string('monitor_dell_path');
			$table->string('color');
			$table->string('price');
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('monitor_dell');
	}

}
