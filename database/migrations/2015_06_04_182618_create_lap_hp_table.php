<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLapHpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lap_hp', function(Blueprint $table)
		{
			$table->string('lap_hp_id')->references('products_id')->on('products');
			$table->string('lap_hp_model_name');
			$table->string('lap_hp_ram');
			$table->string('lap_hp_cpu');
			$table->string('lap_hp_hdd');
			$table->string('lap_hp_screen');
			$table->string('lap_hp_path');
			$table->text('lap_hp_desc'); 
			$table->string('lap_hp_price');
			$table->string('lap_hp_color');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lap_hp');
	}

}
