<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrinterCanonTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('printer_canon', function(Blueprint $table)
		{
			$table->string('printer_canon_id')->references('products_id')->on('products');
			$table->string('printer_canon_model_name');
			$table->string('function');
			$table->string('print_resolution');
			$table->string('print_quality_black');
			$table->string('print_color');
			$table->string('paper_size');
			$table->string('port');
			$table->string('printer_canon_path');
			$table->string('price');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('printer_canon');
	}

}
