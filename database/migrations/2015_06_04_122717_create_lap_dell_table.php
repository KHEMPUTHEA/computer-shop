<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLapDellTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lap_dell', function(Blueprint $table)
		{
			$table->string('lap_dell_id')->references('products_id')->on('products');
			$table->string('lap_dell_model_name');
			$table->string('lap_dell_ram');
			$table->string('lap_dell_cpu');
			$table->string('lap_dell_hdd');
			$table->string('lap_dell_screen');
			$table->string('lap_dell_path');
			$table->text('lap_dell_desc'); 
			$table->string('lap_dell_price');
			$table->string('lap_dell_color');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lap_dell');
	}

}
