<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function(Blueprint $table)
        {
            $table->integer('products_id')->increments()->primary();
            $table->string('products_name');
            $table->string('path');
            $table->string('ram')->nullable();
            $table->string('cpu')->nullable();
            $table->string('hdd')->nullable();
            $table->string('screen')->nullable();
            $table->string('display')->nullable();
            $table->string('resolution')->nullable();
            $table->string('function')->nullable();
            $table->string('print_resolution')->nullable();
            $table->string('print_quality_black')->nullable();
            $table->string('print_color')->nullable();
            $table->string('paper_size')->nullable();
            $table->string('port')->nullable();
            $table->string('brightness')->nullable();
            $table->string('lamp_life')->nullable();
            $table->string('scan')->nullable();
            $table->string('scan_color')->nullable();
            $table->string('processor')->nullable();
            $table->string('storage')->nullable();
            $table->string('memory')->nullable();
            $table->string('products_desc')->nullable();
            $table->integer('quantity');
            $table->string('price');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }

}
