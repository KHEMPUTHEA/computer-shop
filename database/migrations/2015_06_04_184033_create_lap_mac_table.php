<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLapMacTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lap_mac', function(Blueprint $table)
		{
			$table->string('lap_mac_id')->references('products_id')->on('products');
			$table->string('lap_mac_model_name');
			$table->string('lap_mac_ram');
			$table->string('lap_mac_cpu');
			$table->string('lap_mac_hdd');
			$table->string('lap_mac_screen');
			$table->string('lap_mac_path');
			$table->text('lap_mac_desc'); 
			$table->string('lap_mac_price');
			$table->string('lap_mac_color');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lap_mac');
	}

}
