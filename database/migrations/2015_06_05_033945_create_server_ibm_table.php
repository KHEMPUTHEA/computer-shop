<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServerIbmTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('server_ibm', function(Blueprint $table)
		{
			$table->string('server_ibm_id')->references('products_id')->on('products');
			$table->string('server_ibm_model_name');
			$table->string('processor');
			$table->string('storage');
			$table->string('memory');
			$table->string('server_ibm_path');
			$table->string('price');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('server_ibm');
	}

}
