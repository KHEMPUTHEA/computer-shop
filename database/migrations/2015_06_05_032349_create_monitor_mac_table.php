<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitorMacTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('monitor_mac', function(Blueprint $table)
		{
			$table->string('monitor_mac_id')->references('products_id')->on('products');
			$table->string('monitor_mac_model_name');
			$table->string('display');
			$table->string('resolution');
			$table->string('monitor_mac_path');
			$table->string('price');
			$table->string('color');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('monitor_mac');
	}

}
