<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeskHpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('desk_hp', function(Blueprint $table)
		{
			$table->string('desk_hp_id')->references('products_id')->on('products');
			$table->string('desk_hp_model_name');
			$table->string('desk_hp_ram');
			$table->string('desk_hp_cpu');
			$table->string('desk_hp_hdd');
			$table->string('desk_hp_screen');
			$table->string('desk_hp_path');
			$table->text('desk_hp_desc'); 
			$table->string('desk_hp_price');
			$table->string('desk_hp_color');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('desk_hp');
	}

}
