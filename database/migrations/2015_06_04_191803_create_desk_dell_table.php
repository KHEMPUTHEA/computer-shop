<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeskDellTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('desk_dell', function(Blueprint $table)
		{
			$table->string('desk_dell_id')->references('products_id')->on('products');
			$table->string('desk_dell_model_name');
			$table->string('desk_dell_ram');
			$table->string('desk_dell_cpu');
			$table->string('desk_dell_hdd');
			$table->string('desk_dell_screen');
			$table->string('desk_dell_path');
			$table->text('desk_dell_desc'); 
			$table->string('desk_dell_price');
			$table->string('desk_dell_color');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('desk_dell');
	}

}
