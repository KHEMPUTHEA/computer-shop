<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServerDellTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('server_dell', function(Blueprint $table)
		{
			$table->string('server_dell_id')->references('products_id')->on('products');
			$table->string('server_dell_model_name');
			$table->string('processor');
			$table->string('storage');
			$table->string('memory');
			$table->string('server_dell_path');
			$table->string('price');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('server_dell');
	}

}
