<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PrinterDellSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('printer_dell')->insert([
            'printer_dell_id'   => '59',
            'printer_dell_model_name'  => 'Printer Dell Mono Laser1130',
            'function'   => 'Print Only',
            'print_resolution' => '9600 x 2400 dpi',
            'print_quality_black'  => '26ppm',
            'print_color' => '17ppm',
            'paper_size' => 'A4.....',
            'port' => 'USB, PicBridge',
            'printer_dell_path' => '/images/printer/dell/printer_dell_Mono_Laser1130.jpg',
            'price' => '$358'

        ]);
        DB::table('printer_dell')->insert([
            'printer_dell_id'   => '60',
            'printer_dell_model_name'  => 'Printer Dell Mono Laser1130n',
            'function'   => 'Print Only',
            'print_resolution' => '9600 x 2400 dpi',
            'print_quality_black'  => '26ppm',
            'print_color' => '17ppm',
            'paper_size' => 'A4.....',
            'port' => 'USB, PicBridge',
            'printer_dell_path' => '/images/printer/dell/printer_dell_Mono_Laser1130n.jpg',
            'price' => '$358'

        ]);
        DB::table('printer_dell')->insert([
            'printer_dell_id'   => '61',
            'printer_dell_model_name'  => 'Printer Dell Mono LaserB1260dn',
            'function'   => 'Print Only',
            'print_resolution' => '9600 x 2400 dpi',
            'print_quality_black'  => '26ppm',
            'print_color' => '17ppm',
            'paper_size' => 'A4.....',
            'port' => 'USB, PicBridge',
            'printer_dell_path' => '/images/printer/dell/printer_dell_Mono_LaserB1260dn.jpg',
            'price' => '$69'

        ]);
        










    }

}

?>
