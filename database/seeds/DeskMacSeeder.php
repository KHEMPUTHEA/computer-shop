<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DeskMacSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('desk_mac')->insert([
                          'desk_mac_id'   => '21',
                          'desk_mac_model_name'  => 'Desktop Imac',
                          'desk_mac_ram'   => '8GB',
                          'desk_mac_cpu' => 'corei7',
                          'desk_mac_hdd'  => '1TB',
                          'desk_mac_screen' => '15."',
                          'desk_mac_path' => '/images/desktop/mac/desk_imac.jpg',
                          'desk_mac_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_mac_price' => '$1,200',
                          'desk_mac_color' => 'black, white, red'
                          ]);
      DB::table('desk_mac')->insert([
                          'desk_mac_id'   => '22',
                          'desk_mac_model_name'  => 'Desk Imac1',
                          'desk_mac_ram'   => '8GB',
                          'desk_mac_cpu' => 'corei7',
                          'desk_mac_hdd'  => '1TB',
                          'desk_mac_screen' => '15."',
                          'desk_mac_path' => '/images/desktop/mac/desk_imac1.jpg',
                          'desk_mac_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_mac_price' => '$1,200',
                          'desk_mac_color' => 'black, white, red'
                          ]);
      DB::table('desk_mac')->insert([
                          'desk_mac_id'   => '23',
                          'desk_mac_model_name'  => 'Desk Imac Retina',
                          'desk_mac_ram'   => '8GB',
                          'desk_mac_cpu' => 'corei7',
                          'desk_mac_hdd'  => '1TB',
                          'desk_mac_screen' => '15."',
                          'desk_mac_path' => '/images/desktop/mac/desk_imac_retina.jpg',
                          'desk_mac_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_mac_price' => '$1,200',
                          'desk_mac_color' => 'black, white, red'
                          ]);
      DB::table('desk_mac')->insert([
                          'desk_mac_id'   => '24',
                          'desk_mac_model_name'  => 'Mac mini',
                          'desk_mac_ram'   => '8GB',
                          'desk_mac_cpu' => 'corei7',
                          'desk_mac_hdd'  => '1TB',
                          'desk_mac_screen' => '15."',
                          'desk_mac_path' => '/images/desktop/mac/desk_mac_mini.jpg',
                          'desk_mac_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_mac_price' => '$1,200',
                          'desk_mac_color' => 'black, white, red'
                          ]);

      

      
      
      


  }

}

?>
