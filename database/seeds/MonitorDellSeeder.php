<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MonitorDellSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('monitor_dell')->insert([
                          'monitor_dell_id'   => '49',
                          'monitor_dell_model_name'  => 'Monitor Dell E2015HV',
                          'display'   => '23"',
                          'resolution' => '1920 x 1080',
                          'monitor_dell_path'  => '/images/monitor/dell/monitor_dell_E2015HV.jpg',
                          'color' => 'black, white',
                          'price' => '$650'
                          
                          ]);
      DB::table('monitor_dell')->insert([
                          'monitor_dell_id'   => '50',
                          'monitor_dell_model_name'  => 'Monitor Dell S2340T Touch',
                          'display'   => '23"',
                          'resolution' => '1920 x 1080',
                          'monitor_dell_path'  => '/images/monitor/dell/monitor_dell_S2340T_Touch.jpg',
                          'color' => 'black, white',
                          'price' => '$650'
                          
                          ]);
      DB::table('monitor_dell')->insert([
                          'monitor_dell_id'   => '51',
                          'monitor_dell_model_name'  => 'Monitor Dell S2440L',
                          'display'   => '23"',
                          'resolution' => '1920 x 1080',
                          'monitor_dell_path'  => '/images/monitor/dell/monitor_dell_S2440L.jpg',
                          'color' => 'black, white',
                          'price' => '$650'
                          
                          ]);
      DB::table('monitor_dell')->insert([
                          'monitor_dell_id'   => '52',
                          'monitor_dell_model_name'  => 'Monitor Dell U2913WM',
                          'display'   => '23"',
                          'resolution' => '1920 x 1080',
                          'monitor_dell_path'  => '/images/monitor/dell/monitor_dell_U2913WM.jpg',
                          'color' => 'black, white',
                          'price' => '$650'
                          
                          ]);

      
      

      
      
      


  }

}

?>
