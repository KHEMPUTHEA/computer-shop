<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LapMacSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('lap_mac')->insert([
                          'lap_mac_id'   => '44',
                          'lap_mac_model_name'  => 'Mac 2011',
                          'lap_mac_ram'   => '8GB',
                          'lap_mac_cpu' => 'corei7',
                          'lap_mac_hdd'  => '1TB',
                          'lap_mac_screen' => '15."',
                          'lap_mac_path' => '/images/laptop/mac/lap_mac2011.jpg',
                          'lap_mac_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'lap_mac_price' => '$1,200',
                          'lap_mac_color' => 'black, white, red'
                          ]);
      DB::table('lap_mac')->insert([
                          'lap_mac_id'   => '45',
                          'lap_mac_model_name'  => 'Mac 2012',
                          'lap_mac_ram'   => '8GB',
                          'lap_mac_cpu' => 'corei7',
                          'lap_mac_hdd'  => '1TB',
                          'lap_mac_screen' => '15."',
                          'lap_mac_path' => '/images/laptop/mac/lap_mac2012.jpg',
                          'lap_mac_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'lap_mac_price' => '$1,200',
                          'lap_mac_color' => 'black, white, red'
                          ]);
      DB::table('lap_mac')->insert([
                          'lap_mac_id'   => '46',
                          'lap_mac_model_name'  => 'Mac 2013',
                          'lap_mac_ram'   => '8GB',
                          'lap_mac_cpu' => 'corei7',
                          'lap_mac_hdd'  => '1TB',
                          'lap_mac_screen' => '15."',
                          'lap_mac_path' => '/images/laptop/mac/lap_mac2013.jpg',
                          'lap_mac_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'lap_mac_price' => '$1,200',
                          'lap_mac_color' => 'black, white, red'
                          ]);
      DB::table('lap_mac')->insert([
                          'lap_mac_id'   => '47',
                          'lap_mac_model_name'  => 'Mac 2014',
                          'lap_mac_ram'   => '8GB',
                          'lap_mac_cpu' => 'corei7',
                          'lap_mac_hdd'  => '1TB',
                          'lap_mac_screen' => '15."',
                          'lap_mac_path' => '/images/laptop/mac/lap_mac2014.jpg',
                          'lap_mac_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'lap_mac_price' => '$1,200',
                          'lap_mac_color' => 'black, white, red'
                          ]);
      DB::table('lap_mac')->insert([
                          'lap_mac_id'   => '48',
                          'lap_mac_model_name'  => 'Mac Retina Display',
                          'lap_mac_ram'   => '8GB',
                          'lap_mac_cpu' => 'corei7',
                          'lap_mac_hdd'  => '1TB',
                          'lap_mac_screen' => '15."',
                          'lap_mac_path' => '/images/laptop/mac/lap_mac_retina.jpg',
                          'lap_mac_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'lap_mac_price' => '$1,200',
                          'lap_mac_color' => 'black, white, red'
                          ]);

      
      
      


  }

}

?>
