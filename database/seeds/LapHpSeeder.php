<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LapHpSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('lap_hp')->insert([
                          'lap_hp_id'   => '35',
                          'lap_hp_model_name'  => 'Hp Envy',
                          'lap_hp_ram'   => '8GB',
                          'lap_hp_cpu' => 'corei7',
                          'lap_hp_hdd'  => '1TB',
                          'lap_hp_screen' => '15."',
                          'lap_hp_path' => '/images/laptop/hp/lap_hp_envy.jpg',
                          'lap_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'lap_hp_price' => '$1,200',
                          'lap_hp_color' => 'black, white, red'
                          ]);

      DB::table('lap_hp')->insert([
                          'lap_hp_id'   => '36',
                          'lap_hp_model_name'  => 'Hp Omen',
                          'lap_hp_ram'   => '8GB',
                          'lap_hp_cpu' => 'corei7',
                          'lap_hp_hdd'  => '1TB',
                          'lap_hp_screen' => '15."',
                          'lap_hp_path' => '/images/laptop/hp/lap_hp_omen.jpg',
                          'lap_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'lap_hp_price' => '$1,200',
                          'lap_hp_color' => 'black, white, red'
                        ]);
      DB::table('lap_hp')->insert([
                          'lap_hp_id'   => '37',
                          'lap_hp_model_name'  => 'Hp Pavillion dm4',
                          'lap_hp_ram'   => '8GB',
                          'lap_hp_cpu' => 'corei7',
                          'lap_hp_hdd'  => '1TB',
                          'lap_hp_screen' => '15."',
                          'lap_hp_path' => '/images/laptop/hp/lap_hp_pavilliondm4.jpg',
                          'lap_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'lap_hp_price' => '$1,200',
                          'lap_hp_color' => 'black, white, red'
                        ]);
      DB::table('lap_hp')->insert([
                        'lap_hp_id'   => '38',
                        'lap_hp_model_name'  => 'Hp Pavillion dv6',
                        'lap_hp_ram'   => '8GB',
                        'lap_hp_cpu' => 'corei7',
                        'lap_hp_hdd'  => '1TB',
                        'lap_hp_screen' => '15."',
                        'lap_hp_path' => '/images/laptop/hp/lap_hp_pavilliondv6.jpg',
                        'lap_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_hp_price' => '$1,200',
                        'lap_hp_color' => 'black, white, red'
                        ]);
      DB::table('lap_hp')->insert([
                        'lap_hp_id'   => '39',
                        'lap_hp_model_name'  => 'Hp Pavillion dv7',
                        'lap_hp_ram'   => '8GB',
                        'lap_hp_cpu' => 'corei7',
                        'lap_hp_hdd'  => '1TB',
                        'lap_hp_screen' => '15."',
                        'lap_hp_path' => '/images/laptop/hp/lap_hp_pavilliondv7.jpg',
                        'lap_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_hp_price' => '$1,200',
                        'lap_hp_color' => 'black, white, red'
                        ]);
      DB::table('lap_hp')->insert([
                        'lap_hp_id'   => '40',
                        'lap_hp_model_name'  => 'Hp Pavillion g6',
                        'lap_hp_ram'   => '8GB',
                        'lap_hp_cpu' => 'corei7',
                        'lap_hp_hdd'  => '1TB',
                        'lap_hp_screen' => '15."',
                        'lap_hp_path' => '/images/laptop/hp/lap_hp_pavilliong6.jpg',
                        'lap_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_hp_price' => '$1,200',
                        'lap_hp_color' => 'black, white, red'
                        ]);
      DB::table('lap_hp')->insert([
                        'lap_hp_id'   => '41',
                        'lap_hp_model_name'  => 'Hp Pavillion g7',
                        'lap_hp_ram'   => '8GB',
                        'lap_hp_cpu' => 'corei7',
                        'lap_hp_hdd'  => '1TB',
                        'lap_hp_screen' => '15."',
                        'lap_hp_path' => '/images/laptop/hp/lap_hp_pavilliong7.jpg',
                        'lap_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_hp_price' => '$1,200',
                        'lap_hp_color' => 'black, white, red'
                        ]);
      DB::table('lap_hp')->insert([
                        'lap_hp_id'   => '42',
                        'lap_hp_model_name'  => 'Hp Spectre',
                        'lap_hp_ram'   => '8GB',
                        'lap_hp_cpu' => 'corei7',
                        'lap_hp_hdd'  => '1TB',
                        'lap_hp_screen' => '15."',
                        'lap_hp_path' => '/images/laptop/hp/lap_hp_spectre.jpg',
                        'lap_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_hp_price' => '$1,200',
                        'lap_hp_color' => 'black, white, red'
                        ]);
      DB::table('lap_hp')->insert([
                        'lap_hp_id'   => '43',
                        'lap_hp_model_name'  => 'Hp X2',
                        'lap_hp_ram'   => '8GB',
                        'lap_hp_cpu' => 'corei7',
                        'lap_hp_hdd'  => '1TB',
                        'lap_hp_screen' => '15."',
                        'lap_hp_path' => '/images/laptop/hp/lap_hp_x2.jpg',
                        'lap_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_hp_price' => '$1,200',
                        'lap_hp_color' => 'black, white, red'
                        ]);
      
      


  }

}

?>
