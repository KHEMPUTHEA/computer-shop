<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PrinterCanonSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('printer_canon')->insert([
            'printer_canon_id'   => '55',
            'printer_canon_model_name'  => 'Printer Canon PIXAM IX7000',
            'function'   => 'Print Only',
            'print_resolution' => '9600 x 2400 dpi',
            'print_quality_black'  => '26ppm',
            'print_color' => '17ppm',
            'paper_size' => 'A4.....',
            'port' => 'USB, PicBridge',
            'printer_canon_path' => '/images/printer/canon/printer_canon_PIXAM_IX7000.jpg',
            'price' => '$358'

        ]);
        DB::table('printer_canon')->insert([
            'printer_canon_id'   => '56',
            'printer_canon_model_name'  => 'Printer Canon PIXMA iP3680',
            'function'   => 'Print Only',
            'print_resolution' => '9600 x 2400 dpi',
            'print_quality_black'  => '26ppm',
            'print_color' => '17ppm',
            'paper_size' => 'A4.....',
            'port' => 'USB, PicBridge',
            'printer_canon_path' => '/images/printer/canon/printer_canon_PIXMA_iP3680.jpg',
            'price' => '$358'

        ]);
        DB::table('printer_canon')->insert([
            'printer_canon_id'   => '57',
            'printer_canon_model_name'  => 'Printer Canon PIXMA MG2470',
            'function'   => 'Print Only',
            'print_resolution' => '9600 x 2400 dpi',
            'print_quality_black'  => '26ppm',
            'print_color' => '17ppm',
            'paper_size' => 'A4.....',
            'port' => 'USB, PicBridge',
            'printer_canon_path' => '/images/printer/canon/printer_canon_PIXMA_MG2470.jpg',
            'price' => '$69'

        ]);
        DB::table('printer_canon')->insert([
            'printer_canon_id'   => '58',
            'printer_canon_model_name'  => 'Printer Canon PIXMA pro-1',
            'function'   => 'Print Only',
            'print_resolution' => '9600 x 2400 dpi',
            'print_quality_black'  => '26ppm',
            'print_color' => '17ppm',
            'paper_size' => 'A4.....',
            'port' => 'USB, PicBridge',
            'printer_canon_path' => '/images/printer/canon/printer_canon_PIXMA_pro-1.jpg',
            'price' => '$913'

        ]);










    }

}

?>
