<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
 
class UserTableSeeder extends Seeder {
 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vader = DB::table('users')->insert([
                'username'   => 'putheaengineer',
                'email'      => 'putheakhemdeveloper@gmail.com',
                'password'   => Hash::make('putheaengineer'),
                'first_name' => 'Khem',
                'last_name'  => 'Puthea',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]);
 
        DB::table('users')->insert([
                'username'   => 'goodsidesoldier',
                'email'      => 'lightwalker@rebels.com',
                'password'   => Hash::make('hesnotmydad'),
                'first_name' => 'Luke',
                'last_name'  => 'Skywalker',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]);
 
        DB::table('users')->insert([
                'username'   => 'greendemon',
                'email'      => 'dancingsmallman@rebels.com',
                'password'   => Hash::make('yodaIam'),
                'first_name' => 'Yoda',
                'last_name'  => 'Unknown',
                'created_at' => new DateTime(),
                'updated_at' => new DateTime()
            ]);
    }
 
}

?>