<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SpeakerSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('speaker')->insert([
            'speaker_id'   => '81',
            'speaker_model_name'  => 'Speaker1',
            'speaker_path'  => '/images/speaker/speaker1.jpg',
            'price' => '$10'

        ]);

        DB::table('speaker')->insert([
            'speaker_id'   => '82',
            'speaker_model_name'  => 'Speaker2',
            'speaker_path'  => '/images/speaker/speaker2.jpg',
            'price' => '$10'

        ]);

        DB::table('speaker')->insert([
            'speaker_id'   => '83',
            'speaker_model_name'  => 'Speaker3',
            'speaker_path'  => '/images/speaker/speaker3.jpg',
            'price' => '$10'

        ]);

        DB::table('speaker')->insert([
            'speaker_id'   => '84',
            'speaker_model_name'  => 'Speaker',
            'speaker_path'  => '/images/speaker/speaker4.jpg',
            'price' => '$10'

        ]);

        DB::table('speaker')->insert([
            'speaker_id'   => '85',
            'speaker_model_name'  => 'Speaker5',
            'speaker_path'  => '/images/speaker/speaker5.jpg',
            'price' => '$10'

        ]);

        DB::table('speaker')->insert([
            'speaker_id'   => '86',
            'speaker_model_name'  => 'Speaker6',
            'speaker_path'  => '/images/speaker/speaker6.jpg',
            'price' => '$10'

        ]);

        DB::table('speaker')->insert([
            'speaker_id'   => '87',
            'speaker_model_name'  => 'Speaker7',
            'speaker_path'  => '/images/speaker/speaker7.jpg',
            'price' => '$10'

        ]);

        DB::table('speaker')->insert([
            'speaker_id'   => '88',
            'speaker_model_name'  => 'Speaker8',
            'speaker_path'  => '/images/speaker/speaker8.jpg',
            'price' => '$10'

        ]);

        DB::table('speaker')->insert([
            'speaker_id'   => '89',
            'speaker_model_name'  => 'Speaker9',
            'speaker_path'  => '/images/speaker/speaker9.jpg',
            'price' => '$10'

        ]);





    }

}

?>
