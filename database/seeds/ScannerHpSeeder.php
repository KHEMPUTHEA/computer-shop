<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ScannerHpSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('scanner_hp')->insert([
            'scanner_hp_id'   => '74',
            'scanner_hp_model_name'  => 'Scanner Hp G2410C',
            'function'   => 'Scan only',
            'scan' => '9600dpi x 9600dpi',
            'scan_color' => '12 sec/page',
            'paper_size' => 'A4.....',
            'port' => 'USB 2.0',
            'scanner_hp_path'  => '/images/scanner/hp/scanner_hp_G2410C.jpg',
            'price' => '$95'

        ]);

        DB::table('scanner_hp')->insert([
            'scanner_hp_id'   => '75',
            'scanner_hp_model_name'  => 'Scanner Hp G3110',
            'function'   => 'Scan only',
            'scan' => '9600dpi x 9600dpi',
            'scan_color' => '12 sec/page',
            'paper_size' => 'A4.....',
            'port' => 'USB 2.0',
            'scanner_hp_path'  => '/images/scanner/hp/scanner_hp_G3110.jpg',
            'price' => '$145'

        ]);

        DB::table('scanner_hp')->insert([
            'scanner_hp_id'   => '76',
            'scanner_hp_model_name'  => 'Scanner Hp G4010',
            'function'   => 'Scan only',
            'scan' => '9600dpi x 9600dpi',
            'scan_color' => '12 sec/page',
            'paper_size' => 'A4.....',
            'port' => 'USB 2.0',
            'scanner_hp_path'  => '/images/scanner/hp/scanner_hp_G4010.jpg',
            'price' => '$240'

        ]);

        DB::table('scanner_hp')->insert([
            'scanner_hp_id'   => '77',
            'scanner_hp_model_name'  => 'Scanner Hp G8300',
            'function'   => 'Scan only',
            'scan' => '9600dpi x 9600dpi',
            'scan_color' => '12 sec/page',
            'paper_size' => 'A4.....',
            'port' => 'USB 2.0',
            'scanner_hp_path'  => '/images/scanner/hp/scanner_hp_G8300.jpg',
            'price' => '$430'

        ]);







    }

}

?>
