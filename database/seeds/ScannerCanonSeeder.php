<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ScannerCanonSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('scanner_canon')->insert([
            'scanner_canon_id'   => '71',
            'scanner_canon_model_name'  => 'Scanner Canon LiDE 10',
            'function'   => 'Scan only',
            'scan' => '9600dpi x 9600dpi',
            'scan_color' => '12 sec/page',
            'paper_size' => 'A4.....',
            'port' => 'USB 2.0',
            'scanner_canon_path'  => '/images/scanner/canon/scanner_canon_LiDE10.jpg',
            'price' => '$80'

        ]);

        DB::table('scanner_canon')->insert([
            'scanner_canon_id'   => '72',
            'scanner_canon_model_name'  => 'Scanner Canon LiDE210',
            'function'   => 'Scan only',
            'scan' => '9600dpi x 9600dpi',
            'scan_color' => '12 sec/page',
            'paper_size' => 'A4.....',
            'port' => 'USB 2.0',
            'scanner_canon_path'  => '/images/scanner/canon/scanner_canon_LiDE210.jpg',
            'price' => '$116'

        ]);

        DB::table('scanner_canon')->insert([
            'scanner_canon_id'   => '73',
            'scanner_canon_model_name'  => 'Scanner Canon Lide-700f',
            'function'   => 'Scan only',
            'scan' => '9600dpi x 9600dpi',
            'scan_color' => '12 sec/page',
            'paper_size' => 'A4.....',
            'port' => 'USB 2.0',
            'scanner_canon_path'  => '/images/scanner/canon/scanner_canon_Lide-700f.jpg',
            'price' => '$125'

        ]);








    }

}

?>
