<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ServerIbmSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('server_ibm')->insert([
            'server_ibm_id'   => '80',
            'server_ibm_model_name'  => 'Server IBM X100',
            'processor'   => 'Intel Pentium',
            'storage' => '80GB',
            'memory' => '2GB',
            'server_ibm_path'  => '/images/server/ibm/server_ibm_X100.jpg',
            'price' => '$950'

        ]);










    }

}

?>
