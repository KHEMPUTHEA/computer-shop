<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProjectorDellSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projector_dell')->insert([
            'projector_dell_id'   => '62',
            'projector_dell_model_name'  => 'Projector Dell 1210s',
            'brightness'   => '2500 ANSI Lumens',
            'lamp_life' => '3,000 hours',
            'projector_dell_path'  => '/images/projector/dell/projector_dell_1210s.jpg',
            'price' => '$370'

        ]);

        DB::table('projector_dell')->insert([
            'projector_dell_id'   => '63',
            'projector_dell_model_name'  => 'Projector Dell 1220',
            'brightness'   => '2500 ANSI Lumens',
            'lamp_life' => '3,000 hours',
            'projector_dell_path'  => '/images/projector/dell/projector_dell_1220.jpg',
            'price' => '$390'

        ]);

        DB::table('projector_dell')->insert([
            'projector_dell_id'   => '64',
            'projector_dell_model_name'  => 'Projector Dell 1420X',
            'brightness'   => '2500 ANSI Lumens',
            'lamp_life' => '3,000 hours',
            'projector_dell_path'  => '/images/projector/dell/projector_dell_1420X.jpg',
            'price' => '$569'

        ]);

        DB::table('projector_dell')->insert([
            'projector_dell_id'   => '65',
            'projector_dell_model_name'  => 'Projector Dell M115HD',
            'brightness'   => '2500 ANSI Lumens',
            'lamp_life' => '3,000 hours',
            'projector_dell_path'  => '/images/projector/dell/projector_dell_M115HD.jpg',
            'price' => '$549'

        ]);











    }

}

?>
