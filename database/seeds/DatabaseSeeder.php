<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		 $this->call('LapDellSeeder');
		 $this->call('LapHpSeeder');
		 $this->call('LapMacSeeder');
		 $this->call('DeskDellSeeder');
		 $this->call('DeskHpSeeder');
		 $this->call('DeskMacSeeder');
		 $this->call('MonitorDellSeeder');
		 $this->call('MonitorMacSeeder');
		 $this->call('PrinterCanonSeeder');
		 $this->call('PrinterDellSeeder');
		 $this->call('ProjectorDellSeeder');
		 $this->call('ProjectorEpsonSeeder');
		 $this->call('ScannerCanonSeeder');
		 $this->call('ScannerHpSeeder');
		 $this->call('ServerDellSeeder');
		 $this->call('ServerIbmSeeder');
		 $this->call('SpeakerSeeder');
         $this->call('ProductSeeder');
        $this->call('CategorySeeder');
	}

}
