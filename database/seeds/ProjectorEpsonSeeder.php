<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProjectorEpsonSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projector_epson')->insert([
            'projector_epson_id'   => '66',
            'projector_epson_model_name'  => 'Projector Epson EB-S02',
            'brightness'   => '2500 ANSI Lumens',
            'lamp_life' => '3,000 hours',
            'projector_epson_path'  => '/images/projector/epson/projector_epson_EB-S02.jpg',
            'price' => '$380'

        ]);

        DB::table('projector_epson')->insert([
            'projector_epson_id'   => '67',
            'projector_epson_model_name'  => 'Projector Epson EB-S03',
            'brightness'   => '2500 ANSI Lumens',
            'lamp_life' => '3,000 hours',
            'projector_epson_path'  => '/images/projector/epson/projector_epson_EB-S03.jpg',
            'price' => '$400'

        ]);

        DB::table('projector_epson')->insert([
            'projector_epson_id'   => '68',
            'projector_epson_model_name'  => 'Projector Epson EB-S18-SVGA',
            'brightness'   => '2500 ANSI Lumens',
            'lamp_life' => '3,000 hours',
            'projector_epson_path'  => '/images/projector/epson/projector_epson_EB-S18-SVGA.jpg',
            'price' => '$550'

        ]);

        DB::table('projector_epson')->insert([
            'projector_epson_id'   => '69',
            'projector_epson_model_name'  => 'Projector Epson EB-X02',
            'brightness'   => '2500 ANSI Lumens',
            'lamp_life' => '3,000 hours',
            'projector_epson_path'  => '/images/projector/epson/projector_epson_EB-X02.jpg',
            'price' => '$605'

        ]);

         DB::table('projector_epson')->insert([
             'projector_epson_id'   => '70',
             'projector_epson_model_name'  => 'Projector Epson EB-TW8200',
             'brightness'   => '2500 ANSI Lumens',
             'lamp_life' => '3,000 hours',
             'projector_epson_path'  => '/images/projector/epson/projector_epson_EH-TW8200.jpg',
             'price' => '$2,950'

        ]);






    }

}

?>
