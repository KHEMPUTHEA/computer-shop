<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ServerDellSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('server_dell')->insert([
            'server_dell_id'   => '78',
            'server_dell_model_name'  => 'Server Dell PowerEdge R620 Rack Server',
            'processor'   => 'E5-2630',
            'storage' => '900GB',
            'memory' => '8GB',
            'server_dell_path'  => '/images/server/dell/server_dell_PowerEdge_R620_Rack_Server.jpg',
            'price' => '$5,465'

        ]);

        DB::table('server_dell')->insert([
            'server_dell_id'   => '79',
            'server_dell_model_name'  => 'Server Dell PowerEdgeR730',
            'processor'   => 'E5-2630',
            'storage' => '900GB',
            'memory' => '8GB',
            'server_dell_path'  => '/images/server/dell/server_dell_PowerEdgeR730.jpg',
            'price' => '$4,998'

        ]);








    }

}

?>
