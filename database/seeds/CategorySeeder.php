<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CategorySeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            'id' => 1,
            'category_name' => 'Desktop',
            'brand' => 'DELL',
            'product_id' => 1
        ]);

        DB::table('category')->insert([
            'id' => 2,
            'category_name' => 'Desktop',
            'brand' => 'DELL',
            'product_id' => 2
        ]);

        DB::table('category')->insert([
            'id' => 3,
            'category_name' => 'Desktop',
            'brand' => 'DELL',
            'product_id' => 3
        ]);

        DB::table('category')->insert([
            'id' => 4,
            'category_name' => 'Desktop',
            'brand' => 'DELL',
            'product_id' => 4
        ]);

        DB::table('category')->insert([
            'id' => 5,
            'category_name' => 'Desktop',
            'brand' => 'DELL',
            'product_id' => 5
        ]);

        DB::table('category')->insert([
            'id' => 6,
            'category_name' => 'Desktop',
            'brand' => 'DELL',
            'product_id' => 6
        ]);

        DB::table('category')->insert([
            'id' => 7,
            'category_name' => 'Desktop',
            'brand' => 'DELL',
            'product_id' => 7
        ]);

        DB::table('category')->insert([
            'id' => 8,
            'category_name' => 'Desktop',
            'brand' => 'DELL',
            'product_id' => 8
        ]);

        DB::table('category')->insert([
            'id' => 9,
            'category_name' => 'Desktop',
            'brand' => 'DELL',
            'product_id' => 9
        ]);

        DB::table('category')->insert([
            'id' => 10,
            'category_name' => 'Desktop',
            'brand' => 'DELL',
            'product_id' => 10
        ]);

        DB::table('category')->insert([
            'id' => 11,
            'category_name' => 'Desktop',
            'brand' => 'Hp',
            'product_id' => 11
        ]);

        DB::table('category')->insert([
            'id' => 12,
            'category_name' => 'Desktop',
            'brand' => 'Hp',
            'product_id' => 12
        ]);

        DB::table('category')->insert([
            'id' => 13,
            'category_name' => 'Desktop',
            'brand' => 'Hp',
            'product_id' => 13
        ]);
        DB::table('category')->insert([
            'id' => 14,
            'category_name' => 'Desktop',
            'brand' => 'Hp',
            'product_id' => 14
        ]);
        DB::table('category')->insert([
            'id' => 15,
            'category_name' => 'Desktop',
            'brand' => 'Hp',
            'product_id' => 15
        ]);
        DB::table('category')->insert([
            'id' => 16,
            'category_name' => 'Desktop',
            'brand' => 'Hp',
            'product_id' => 16
        ]);
        DB::table('category')->insert([
            'id' => 17,
            'category_name' => 'Desktop',
            'brand' => 'Hp',
            'product_id' => 17
        ]);
        DB::table('category')->insert([
            'id' => 18,
            'category_name' => 'Desktop',
            'brand' => 'Hp',
            'product_id' => 18
        ]);
        DB::table('category')->insert([
            'id' => 19,
            'category_name' => 'Desktop',
            'brand' => 'Hp',
            'product_id' => 19
        ]);
        DB::table('category')->insert([
            'id' => 20,
            'category_name' => 'Desktop',
            'brand' => 'Hp',
            'product_id' => 20
        ]);
        DB::table('category')->insert([
            'id' => 21,
            'category_name' => 'Desktop',
            'brand' => 'Mac',
            'product_id' => 21
        ]);
        DB::table('category')->insert([
            'id' => 22,
            'category_name' => 'Desktop',
            'brand' => 'Mac',
            'product_id' => 22
        ]);
        DB::table('category')->insert([
            'id' => 23,
            'category_name' => 'Desktop',
            'brand' => 'Mac',
            'product_id' => 23
        ]);
        DB::table('category')->insert([
            'id' => 24,
            'category_name' => 'Desktop',
            'brand' => 'Mac',
            'product_id' => 24
        ]);
        DB::table('category')->insert([
            'id' => 25,
            'category_name' => 'Laptop',
            'brand' => 'DELL',
            'product_id' => 25
        ]);
        DB::table('category')->insert([
            'id' => 26,
            'category_name' => 'Laptop',
            'brand' => 'DELL',
            'product_id' => 26
        ]);
        DB::table('category')->insert([
            'id' => 27,
            'category_name' => 'Laptop',
            'brand' => 'DELL',
            'product_id' => 27
        ]);
        DB::table('category')->insert([
            'id' => 28,
            'category_name' => 'Laptop',
            'brand' => 'DELL',
            'product_id' => 28
        ]);
        DB::table('category')->insert([
            'id' => 29,
            'category_name' => 'Laptop',
            'brand' => 'DELL',
            'product_id' => 29
        ]);
        DB::table('category')->insert([
            'id' => 30,
            'category_name' => 'Laptop',
            'brand' => 'DELL',
            'product_id' => 30
        ]);
        DB::table('category')->insert([
            'id' => 31,
            'category_name' => 'Laptop',
            'brand' => 'DELL',
            'product_id' => 31
        ]);
        DB::table('category')->insert([
            'id' => 32,
            'category_name' => 'Laptop',
            'brand' => 'DELL',
            'product_id' => 32
        ]);
        DB::table('category')->insert([
            'id' => 33,
            'category_name' => 'Laptop',
            'brand' => 'DELL',
            'product_id' => 33
        ]);
        DB::table('category')->insert([
            'id' => 34,
            'category_name' => 'Laptop',
            'brand' => 'DELL',
            'product_id' => 34
        ]);

        DB::table('category')->insert([
            'id' => 35,
            'category_name' => 'Laptop',
            'brand' => 'Hp',
            'product_id' => 35
        ]);
        DB::table('category')->insert([
            'id' => 36,
            'category_name' => 'Laptop',
            'brand' => 'Hp',
            'product_id' => 36
        ]);
        DB::table('category')->insert([
            'id' => 37,
            'category_name' => 'Laptop',
            'brand' => 'Hp',
            'product_id' => 37
        ]);
        DB::table('category')->insert([
            'id' => 38,
            'category_name' => 'Laptop',
            'brand' => 'Hp',
            'product_id' => 38
        ]);
        DB::table('category')->insert([
            'id' => 39,
            'category_name' => 'Laptop',
            'brand' => 'Hp',
            'product_id' => 39
        ]);
        DB::table('category')->insert([
            'id' => 40,
            'category_name' => 'Laptop',
            'brand' => 'Hp',
            'product_id' => 40
        ]);
        DB::table('category')->insert([
            'id' => 41,
            'category_name' => 'Laptop',
            'brand' => 'Hp',
            'product_id' => 41
        ]);
        DB::table('category')->insert([
            'id' => 42,
            'category_name' => 'Laptop',
            'brand' => 'Hp',
            'product_id' => 42
        ]);
        DB::table('category')->insert([
            'id' => 43,
            'category_name' => 'Laptop',
            'brand' => 'Hp',
            'product_id' => 43
        ]);
        DB::table('category')->insert([
            'id' => 44,
            'category_name' => 'Laptop',
            'brand' => 'Mac',
            'product_id' => 44
        ]);
        DB::table('category')->insert([
            'id' => 45,
            'category_name' => 'Laptop',
            'brand' => 'Mac',
            'product_id' => 45
        ]);
        DB::table('category')->insert([
            'id' => 46,
            'category_name' => 'Laptop',
            'brand' => 'Mac',
            'product_id' => 46
        ]);
        DB::table('category')->insert([
            'id' => 47,
            'category_name' => 'Laptop',
            'brand' => 'Mac',
            'product_id' => 47
        ]);
        DB::table('category')->insert([
            'id' => 48,
            'category_name' => 'Laptop',
            'brand' => 'Mac',
            'product_id' => 48
        ]);
        DB::table('category')->insert([
            'id' => 49,
            'category_name' => 'Monitor',
            'brand' => 'DELL',
            'product_id' => 49
        ]);
        DB::table('category')->insert([
            'id' => 50,
            'category_name' => 'Monitor',
            'brand' => 'DELL',
            'product_id' => 50
        ]);
        DB::table('category')->insert([
            'id' => 51,
            'category_name' => 'Monitor',
            'brand' => 'DELL',
            'product_id' => 51
        ]);
        DB::table('category')->insert([
            'id' => 52,
            'category_name' => 'Monitor',
            'brand' => 'DELL',
            'product_id' => 52
        ]);
        DB::table('category')->insert([
            'id' => 53,
            'category_name' => 'Monitor',
            'brand' => 'Mac',
            'product_id' => 53
        ]);
        DB::table('category')->insert([
            'id' => 54,
            'category_name' => 'Monitor',
            'brand' => 'Mac',
            'product_id' => 54
        ]);
        DB::table('category')->insert([
            'id' => 55,
            'category_name' => 'Printer',
            'brand' => 'Canon',
            'product_id' => 55
        ]);
        DB::table('category')->insert([
            'id' => 56,
            'category_name' => 'Printer',
            'brand' => 'Canon',
            'product_id' => 56
        ]);
        DB::table('category')->insert([
            'id' => 57,
            'category_name' => 'Printer',
            'brand' => 'Canon',
            'product_id' => 57
        ]);
        DB::table('category')->insert([
            'id' => 58,
            'category_name' => 'Printer',
            'brand' => 'Canon',
            'product_id' => 58
        ]);
        DB::table('category')->insert([
            'id' => 59,
            'category_name' => 'Printer',
            'brand' => 'DELL',
            'product_id' => 59
        ]);
        DB::table('category')->insert([
            'id' => 60,
            'category_name' => 'Printer',
            'brand' => 'DELL',
            'product_id' => 60
        ]);
        DB::table('category')->insert([
            'id' => 61,
            'category_name' => 'Printer',
            'brand' => 'DELL',
            'product_id' => 61
        ]);
        DB::table('category')->insert([
            'id' => 62,
            'category_name' => 'Projector',
            'brand' => 'DELL',
            'product_id' => 62
        ]);
        DB::table('category')->insert([
            'id' => 63,
            'category_name' => 'Projector',
            'brand' => 'DELL',
            'product_id' => 63
        ]);
        DB::table('category')->insert([
            'id' => 64,
            'category_name' => 'Projector',
            'brand' => 'DELL',
            'product_id' => 64
        ]);
        DB::table('category')->insert([
            'id' => 65,
            'category_name' => 'Projector',
            'brand' => 'DELL',
            'product_id' => 65
        ]);
        DB::table('category')->insert([
            'id' => 66,
            'category_name' => 'Projector',
            'brand' => 'EPSON',
            'product_id' => 66
        ]);
        DB::table('category')->insert([
            'id' => 67,
            'category_name' => 'Projector',
            'brand' => 'EPSON',
            'product_id' => 67
        ]);
        DB::table('category')->insert([
            'id' => 68,
            'category_name' => 'Projector',
            'brand' => 'EPSON',
            'product_id' => 68
        ]);
        DB::table('category')->insert([
            'id' => 69,
            'category_name' => 'Projector',
            'brand' => 'EPSON',
            'product_id' => 69
        ]);
        DB::table('category')->insert([
            'id' => 70,
            'category_name' => 'Projector',
            'brand' => 'EPSON',
            'product_id' => 70
        ]);
        DB::table('category')->insert([
            'id' => 71,
            'category_name' => 'Scanner',
            'brand' => 'Canon',
            'product_id' => 71
        ]);
        DB::table('category')->insert([
            'id' => 72,
            'category_name' => 'Scanner',
            'brand' => 'Canon',
            'product_id' => 72
        ]);
        DB::table('category')->insert([
            'id' => 73,
            'category_name' => 'Scanner',
            'brand' => 'Canon',
            'product_id' => 73
        ]);
        DB::table('category')->insert([
            'id' => 74,
            'category_name' => 'Scanner',
            'brand' => 'Hp',
            'product_id' => 74
        ]);
        DB::table('category')->insert([
            'id' => 75,
            'category_name' => 'Scanner',
            'brand' => 'Hp',
            'product_id' => 75
        ]);
        DB::table('category')->insert([
            'id' => 76,
            'category_name' => 'Scanner',
            'brand' => 'Hp',
            'product_id' => 76
        ]);
        DB::table('category')->insert([
            'id' => 77,
            'category_name' => 'Scanner',
            'brand' => 'Hp',
            'product_id' => 77
        ]);
        DB::table('category')->insert([
            'id' => 78,
            'category_name' => 'Server',
            'brand' => 'DELL',
            'product_id' => 78
        ]);
        DB::table('category')->insert([
            'id' => 79,
            'category_name' => 'Server',
            'brand' => 'DELL',
            'product_id' => 79
        ]);
        DB::table('category')->insert([
            'id' => 80,
            'category_name' => 'Server',
            'brand' => 'IBM',
            'product_id' => 80
        ]);
        DB::table('category')->insert([
            'id' => 81,
            'category_name' => 'Speaker',
            'brand' => 'none',
            'product_id' => 81
        ]);
        DB::table('category')->insert([
            'id' => 82,
            'category_name' => 'Speaker',
            'brand' => 'none',
            'product_id' => 82
        ]);
        DB::table('category')->insert([
            'id' => 83,
            'category_name' => 'Speaker',
            'brand' => 'none',
            'product_id' => 83
        ]);
        DB::table('category')->insert([
            'id' => 84,
            'category_name' => 'Speaker',
            'brand' => 'none',
            'product_id' => 84
        ]);
        DB::table('category')->insert([
            'id' => 85,
            'category_name' => 'Speaker',
            'brand' => 'none',
            'product_id' => 85
        ]);
        DB::table('category')->insert([
            'id' => 86,
            'category_name' => 'Speaker',
            'brand' => 'none',
            'product_id' => 86
        ]);
        DB::table('category')->insert([
            'id' => 87,
            'category_name' => 'Speaker',
            'brand' => 'none',
            'product_id' => 87
        ]);
        DB::table('category')->insert([
            'id' => 88,
            'category_name' => 'Speaker',
            'brand' => 'none',
            'product_id' => 88
        ]);
        DB::table('category')->insert([
            'id' => 89,
            'category_name' => 'Speaker',
            'brand' => 'none',
            'product_id' => 89
        ]);
        DB::table('category')->insert([
            'id' => 90,
            'category_name' => 'carousel',
            'brand' => 'none',
            'product_id' => 90
        ]);

        DB::table('category')->insert([
            'id' => 91,
            'category_name' => 'carousel',
            'brand' => 'none',
            'product_id' => 91
        ]);

        DB::table('category')->insert([
            'id' => 92,
            'category_name' => 'carousel',
            'brand' => 'none',
            'product_id' => 92
        ]);





    }

}

?>
