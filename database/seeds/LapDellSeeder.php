<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LapDellSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('lap_dell')->insert([
                          'lap_dell_id'   => '25',
                          'lap_dell_model_name'  => 'Dell Alienware',
                          'lap_dell_ram'   => '8GB',
                          'lap_dell_cpu' => 'corei7',
                          'lap_dell_hdd'  => '1TB',
                          'lap_dell_screen' => '15."',
                          'lap_dell_path' => '/images/laptop/dell/lap_dell_alienware.jpg',
                          'lap_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'lap_dell_price' => '$1,200',
                          'lap_dell_color' => 'black, white, red'
                          ]);

      DB::table('lap_dell')->insert([
                          'lap_dell_id'   => '26',
                          'lap_dell_model_name'  => 'Dell Chromebook',
                          'lap_dell_ram'   => '8GB',
                          'lap_dell_cpu' => 'corei7',
                          'lap_dell_hdd'  => '1TB',
                          'lap_dell_screen' => '15."',
                          'lap_dell_path' => '/images/laptop/dell/lap_dell_chromebook.jpg',
                          'lap_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'lap_dell_price' => '$1,200',
                          'lap_dell_color' => 'black, white, red'
                        ]);
      DB::table('lap_dell')->insert([
                          'lap_dell_id'   => '27',
                          'lap_dell_model_name'  => 'Dell Flip',
                          'lap_dell_ram'   => '8GB',
                          'lap_dell_cpu' => 'corei7',
                          'lap_dell_hdd'  => '1TB',
                          'lap_dell_screen' => '15."',
                          'lap_dell_path' => '/images/laptop/dell/lap_dell_flip.jpg',
                          'lap_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'lap_dell_price' => '$1,200',
                          'lap_dell_color' => 'black, white, red'
                        ]);
      DB::table('lap_dell')->insert([
                        'lap_dell_id'   => '28',
                        'lap_dell_model_name'  => 'Dell Inspiron',
                        'lap_dell_ram'   => '8GB',
                        'lap_dell_cpu' => 'corei7',
                        'lap_dell_hdd'  => '1TB',
                        'lap_dell_screen' => '15."',
                        'lap_dell_path' => '/images/laptop/dell/lap_dell_inspiron.jpg',
                        'lap_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_dell_price' => '$1,200',
                        'lap_dell_color' => 'black, white, red'
                        ]);
      DB::table('lap_dell')->insert([
                        'lap_dell_id'   => '29',
                        'lap_dell_model_name'  => 'Dell Inspiron14',
                        'lap_dell_ram'   => '8GB',
                        'lap_dell_cpu' => 'corei7',
                        'lap_dell_hdd'  => '1TB',
                        'lap_dell_screen' => '15."',
                        'lap_dell_path' => '/images/laptop/dell/lap_dell_inspiron14.jpg',
                        'lap_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_dell_price' => '$1,200',
                        'lap_dell_color' => 'black, white, red'
                        ]);
      DB::table('lap_dell')->insert([
                        'lap_dell_id'   => '30',
                        'lap_dell_model_name'  => 'Dell Inspiron17',
                        'lap_dell_ram'   => '8GB',
                        'lap_dell_cpu' => 'corei7',
                        'lap_dell_hdd'  => '1TB',
                        'lap_dell_screen' => '15."',
                        'lap_dell_path' => '/images/laptop/dell/lap_dell_inspiron17.jpg',
                        'lap_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_dell_price' => '$1,200',
                        'lap_dell_color' => 'black, white, red'
                        ]);
      DB::table('lap_dell')->insert([
                        'lap_dell_id'   => '31',
                        'lap_dell_model_name'  => 'Dell Inspiron700',
                        'lap_dell_ram'   => '8GB',
                        'lap_dell_cpu' => 'corei7',
                        'lap_dell_hdd'  => '1TB',
                        'lap_dell_screen' => '15."',
                        'lap_dell_path' => '/images/laptop/dell/lap_dell_inspiron700.jpg',
                        'lap_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_dell_price' => '$1,200',
                        'lap_dell_color' => 'black, white, red'
                        ]);
      DB::table('lap_dell')->insert([
                        'lap_dell_id'   => '32',
                        'lap_dell_model_name'  => 'Dell Inspiron5000',
                        'lap_dell_ram'   => '8GB',
                        'lap_dell_cpu' => 'corei7',
                        'lap_dell_hdd'  => '1TB',
                        'lap_dell_screen' => '15."',
                        'lap_dell_path' => '/images/laptop/dell/lap_dell_inspiron5000.jpg',
                        'lap_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_dell_price' => '$1,200',
                        'lap_dell_color' => 'black, white, red'
                        ]);
      DB::table('lap_dell')->insert([
                        'lap_dell_id'   => '33',
                        'lap_dell_model_name'  => 'Dell Inspiron touch',
                        'lap_dell_ram'   => '8GB',
                        'lap_dell_cpu' => 'corei7',
                        'lap_dell_hdd'  => '1TB',
                        'lap_dell_screen' => '15."',
                        'lap_dell_path' => '/images/laptop/dell/lap_dell_inspirontouch.jpg',
                        'lap_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_dell_price' => '$1,200',
                        'lap_dell_color' => 'black, white, red'
                        ]);
      DB::table('lap_dell')->insert([
                        'lap_dell_id'   => '34',
                        'lap_dell_model_name'  => 'Dell Inspiron XPS',
                        'lap_dell_ram'   => '8GB',
                        'lap_dell_cpu' => 'corei7',
                        'lap_dell_hdd'  => '1TB',
                        'lap_dell_screen' => '15."',
                        'lap_dell_path' => '/images/laptop/dell/lap_dell_inspironxps.jpg',
                        'lap_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                        'lap_dell_price' => '$1,200',
                        'lap_dell_color' => 'black, white, red'
                        ]);
      


  }

}

?>
