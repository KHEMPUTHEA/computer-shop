<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DeskDellSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('desk_dell')->insert([
                          'desk_dell_id'   => '1',
                          'desk_dell_model_name'  => 'Dell Dimension 2011',
                          'desk_dell_ram'   => '8GB',
                          'desk_dell_cpu' => 'corei7',
                          'desk_dell_hdd'  => '1TB',
                          'desk_dell_screen' => '15."',
                          'desk_dell_path' => '/images/desktop/dell/desk_dell_dimension_2011.jpg',
                          'desk_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_dell_price' => '$1,200',
                          'desk_dell_color' => 'black, white, red'
                          ]);
      DB::table('desk_dell')->insert([
                          'desk_dell_id'   => '2',
                          'desk_dell_model_name'  => 'Dell Dimension 5100',
                          'desk_dell_ram'   => '8GB',
                          'desk_dell_cpu' => 'corei7',
                          'desk_dell_hdd'  => '1TB',
                          'desk_dell_screen' => '15."',
                          'desk_dell_path' => '/images/desktop/dell/desk_dell_dimension_5100.jpg',
                          'desk_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_dell_price' => '$1,200',
                          'desk_dell_color' => 'black, white, red'
                          ]);
      DB::table('desk_dell')->insert([
                          'desk_dell_id'   => '3',
                          'desk_dell_model_name'  => 'Dell Inspiron 99',
                          'desk_dell_ram'   => '8GB',
                          'desk_dell_cpu' => 'corei7',
                          'desk_dell_hdd'  => '1TB',
                          'desk_dell_screen' => '15."',
                          'desk_dell_path' => '/images/desktop/dell/desk_dell_inspiron99.jpg',
                          'desk_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_dell_price' => '$1,200',
                          'desk_dell_color' => 'black, white, red'
                          ]);
      DB::table('desk_dell')->insert([
                          'desk_dell_id'   => '4',
                          'desk_dell_model_name'  => 'Dell Inspiron 660',
                          'desk_dell_ram'   => '8GB',
                          'desk_dell_cpu' => 'corei7',
                          'desk_dell_hdd'  => '1TB',
                          'desk_dell_screen' => '15."',
                          'desk_dell_path' => '/images/desktop/dell/desk_dell_inspiron660.jpg',
                          'desk_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_dell_price' => '$1,200',
                          'desk_dell_color' => 'black, white, red'
                          ]);
      DB::table('desk_dell')->insert([
                          'desk_dell_id'   => '5',
                          'desk_dell_model_name'  => 'Dell Inspiron 662',
                          'desk_dell_ram'   => '8GB',
                          'desk_dell_cpu' => 'corei7',
                          'desk_dell_hdd'  => '1TB',
                          'desk_dell_screen' => '15."',
                          'desk_dell_path' => '/images/desktop/dell/desk_dell_inspiron662.jpg',
                          'desk_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_dell_price' => '$1,200',
                          'desk_dell_color' => 'black, white, red'
                          ]);
      DB::table('desk_dell')->insert([
                          'desk_dell_id'   => '6',
                          'desk_dell_model_name'  => 'Dell Inspiron 6661',
                          'desk_dell_ram'   => '8GB',
                          'desk_dell_cpu' => 'corei7',
                          'desk_dell_hdd'  => '1TB',
                          'desk_dell_screen' => '15."',
                          'desk_dell_path' => '/images/desktop/dell/desk_dell_inspiron6661.jpg',
                          'desk_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_dell_price' => '$1,200',
                          'desk_dell_color' => 'black, white, red'
                          ]);
      DB::table('desk_dell')->insert([
                          'desk_dell_id'   => '7',
                          'desk_dell_model_name'  => 'Dell Inspiron 222',
                          'desk_dell_ram'   => '8GB',
                          'desk_dell_cpu' => 'corei7',
                          'desk_dell_hdd'  => '1TB',
                          'desk_dell_screen' => '15."',
                          'desk_dell_path' => '/images/desktop/dell/desk_dell_inspiron_222.jpg',
                          'desk_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_dell_price' => '$1,200',
                          'desk_dell_color' => 'black, white, red'
                          ]);
      DB::table('desk_dell')->insert([
                          'desk_dell_id'   => '8',
                          'desk_dell_model_name'  => 'Dell Inspiron 999',
                          'desk_dell_ram'   => '8GB',
                          'desk_dell_cpu' => 'corei7',
                          'desk_dell_hdd'  => '1TB',
                          'desk_dell_screen' => '15."',
                          'desk_dell_path' => '/images/desktop/dell/desk_dell_inspiron_999.jpg',
                          'desk_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_dell_price' => '$1,200',
                          'desk_dell_color' => 'black, white, red'
                          ]);
      DB::table('desk_dell')->insert([
                          'desk_dell_id'   => '9',
                          'desk_dell_model_name'  => 'Dell Micro Desktop',
                          'desk_dell_ram'   => '8GB',
                          'desk_dell_cpu' => 'corei7',
                          'desk_dell_hdd'  => '1TB',
                          'desk_dell_screen' => '15."',
                          'desk_dell_path' => '/images/desktop/dell/desk_dell_micro_desktop.jpg',
                          'desk_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_dell_price' => '$1,200',
                          'desk_dell_color' => 'black, white, red'
                          ]);
      DB::table('desk_dell')->insert([
                          'desk_dell_id'   => '10',
                          'desk_dell_model_name'  => 'Dell Vostro 220',
                          'desk_dell_ram'   => '8GB',
                          'desk_dell_cpu' => 'corei7',
                          'desk_dell_hdd'  => '1TB',
                          'desk_dell_screen' => '15."',
                          'desk_dell_path' => '/images/desktop/dell/desk_dell_vostro220.jpg',
                          'desk_dell_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_dell_price' => '$1,200',
                          'desk_dell_color' => 'black, white, red'
                          ]);

      
      
      


  }

}

?>
