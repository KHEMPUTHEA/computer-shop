<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DeskHpSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('desk_hp')->insert([
                          'desk_hp_id'   => '11',
                          'desk_hp_model_name'  => 'Hp Pavillion p6',
                          'desk_hp_ram'   => '8GB',
                          'desk_hp_cpu' => 'corei7',
                          'desk_hp_hdd'  => '1TB',
                          'desk_hp_screen' => '15."',
                          'desk_hp_path' => '/images/desktop/hp/desk_hp_pavillion_p6.jpg',
                          'desk_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_hp_price' => '$1,200',
                          'desk_hp_color' => 'black, white, red'
                          ]);
      DB::table('desk_hp')->insert([
                          'desk_hp_id'   => '12',
                          'desk_hp_model_name'  => 'Hp Pavillion p7',
                          'desk_hp_ram'   => '8GB',
                          'desk_hp_cpu' => 'corei7',
                          'desk_hp_hdd'  => '1TB',
                          'desk_hp_screen' => '15."',
                          'desk_hp_path' => '/images/desktop/hp/desk_hp_pavillion_p7.jpg',
                          'desk_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_hp_price' => '$1,200',
                          'desk_hp_color' => 'black, white, red'
                          ]);
      DB::table('desk_hp')->insert([
                          'desk_hp_id'   => '13',
                          'desk_hp_model_name'  => 'Hp Pavillion p8',
                          'desk_hp_ram'   => '8GB',
                          'desk_hp_cpu' => 'corei7',
                          'desk_hp_hdd'  => '1TB',
                          'desk_hp_screen' => '15."',
                          'desk_hp_path' => '/images/desktop/hp/desk_hp_pavillion_p8.jpg',
                          'desk_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_hp_price' => '$1,200',
                          'desk_hp_color' => 'black, white, red'
                          ]);
      DB::table('desk_hp')->insert([
                          'desk_hp_id'   => '14',
                          'desk_hp_model_name'  => 'Hp Pavillion p9',
                          'desk_hp_ram'   => '8GB',
                          'desk_hp_cpu' => 'corei7',
                          'desk_hp_hdd'  => '1TB',
                          'desk_hp_screen' => '15."',
                          'desk_hp_path' => '/images/desktop/hp/desk_hp_pavillion_p9.jpg',
                          'desk_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_hp_price' => '$1,200',
                          'desk_hp_color' => 'black, white, red'
                          ]);
      DB::table('desk_hp')->insert([
                          'desk_hp_id'   => '15',
                          'desk_hp_model_name'  => 'Hp Pavillion p10',
                          'desk_hp_ram'   => '8GB',
                          'desk_hp_cpu' => 'corei7',
                          'desk_hp_hdd'  => '1TB',
                          'desk_hp_screen' => '15."',
                          'desk_hp_path' => '/images/desktop/hp/desk_hp_pavillion_p10.jpg',
                          'desk_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_hp_price' => '$1,200',
                          'desk_hp_color' => 'black, white, red'
                          ]);
      DB::table('desk_hp')->insert([
                          'desk_hp_id'   => '16',
                          'desk_hp_model_name'  => 'Hp Pavillion p11',
                          'desk_hp_ram'   => '8GB',
                          'desk_hp_cpu' => 'corei7',
                          'desk_hp_hdd'  => '1TB',
                          'desk_hp_screen' => '15."',
                          'desk_hp_path' => '/images/desktop/hp/desk_hp_pavillion_p11.jpg',
                          'desk_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_hp_price' => '$1,200',
                          'desk_hp_color' => 'black, white, red'
                          ]);
      DB::table('desk_hp')->insert([
                          'desk_hp_id'   => '17',
                          'desk_hp_model_name'  => 'Hp Pavillion p12',
                          'desk_hp_ram'   => '8GB',
                          'desk_hp_cpu' => 'corei7',
                          'desk_hp_hdd'  => '1TB',
                          'desk_hp_screen' => '15."',
                          'desk_hp_path' => '/images/desktop/hp/desk_hp_pavillion_p12.jpg',
                          'desk_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_hp_price' => '$1,200',
                          'desk_hp_color' => 'black, white, red'
                          ]);
      DB::table('desk_hp')->insert([
                          'desk_hp_id'   => '18',
                          'desk_hp_model_name'  => 'Hp Pavillion p13',
                          'desk_hp_ram'   => '8GB',
                          'desk_hp_cpu' => 'corei7',
                          'desk_hp_hdd'  => '1TB',
                          'desk_hp_screen' => '15."',
                          'desk_hp_path' => '/images/desktop/hp/desk_hp_pavillion_p13.jpg',
                          'desk_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_hp_price' => '$1,200',
                          'desk_hp_color' => 'black, white, red'
                          ]);
      DB::table('desk_hp')->insert([
                          'desk_hp_id'   => '19',
                          'desk_hp_model_name'  => 'Hp Pavillion p14',
                          'desk_hp_ram'   => '8GB',
                          'desk_hp_cpu' => 'corei7',
                          'desk_hp_hdd'  => '1TB',
                          'desk_hp_screen' => '15."',
                          'desk_hp_path' => '/images/desktop/hp/desk_hp_pavillion_p14.jpg',
                          'desk_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_hp_price' => '$1,200',
                          'desk_hp_color' => 'black, white, red'
                          ]);
      DB::table('desk_hp')->insert([
                          'desk_hp_id'   => '20',
                          'desk_hp_model_name'  => 'Hp Pavillion p15',
                          'desk_hp_ram'   => '8GB',
                          'desk_hp_cpu' => 'corei7',
                          'desk_hp_hdd'  => '1TB',
                          'desk_hp_screen' => '15."',
                          'desk_hp_path' => '/images/desktop/hp/desk_hp_pavillion_p15.jpg',
                          'desk_hp_desc' => 'This is one of the best computer for gaming and graphic design.',
                          'desk_hp_price' => '$1,200',
                          'desk_hp_color' => 'black, white, red'
                          ]);

      

      
      
      


  }

}

?>
