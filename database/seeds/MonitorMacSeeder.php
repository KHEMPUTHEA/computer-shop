<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MonitorMacSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('monitor_mac')->insert([
                          'monitor_mac_id'   => '53',
                          'monitor_mac_model_name'  => 'Monitor Mac MC007ZPA',
                          'display'   => '23"',
                          'resolution' => '1920 x 1080',
                          'monitor_mac_path'  => '/images/monitor/dell/monitor_mac_MC007ZPA.jpg',
                          'color' => 'black, white',
                          'price' => '$650'
                          
                          ]);
      DB::table('monitor_mac')->insert([
                          'monitor_mac_id'   => '54',
                          'monitor_mac_model_name'  => 'Monitor Mac MC914ZPA',
                          'display'   => '23"',
                          'resolution' => '1920 x 1080',
                          'monitor_mac_path'  => '/images/monitor/dell/monitor_mac_MC914ZPA.jpg',
                          'color' => 'black, white',
                          'price' => '$650'
                          
                          ]);
      

      
      

      
      
      


  }

}

?>
