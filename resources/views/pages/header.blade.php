<!DOCTYE html>
<html>
    <head>
        <meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Techno Computer</title>
        <link rel="stylesheet" type="text/css" href="{{asset('/css/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('/css/mystyle.css')}}">

        <script type="text/javascript" src="{{asset('/js/jquery.js')}}"></script>
        <script type="text/javascript" src="{{asset('/js/myJquery.js')}}"></script>
        <script type="text/javascript" src="{{asset('/js/ajax.js')}}"></script>
        <script type="text/javascript" src="{{asset('/js/bootstrap.js')}}"></script>
    </head>

    <body id="b">

        <!-- ======================= Start Header ============================================== -->
        <!-- ======== Start Navigation Bar ======================== -->
        <nav class="navbar-default navbar-fixed-top" id="mybar">
            <div class="container">
                <div class="navbar-header">
                    <button id="my-tog" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#my-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" id="myBrand" href="#">Techno Computer</a>
                    <div style="float: left">
                        <span class="glyphicon glyphicon-search" id="img-sch"></span>
                    </div>
                </div>

                <div class="collapse navbar-collapse" id="my-navbar-collapse">
                    <ul class="nav navbar-nav navbar-right" id="myul">
                        <li>
                            <a id="home" href="{{asset('index')}}">HOME</a>
                        </li>
                        <li>
                            <a id="catalogue" href="{{asset('catalogue')}}">CATALOGUE</a>
                        </li>
                        <li>
                            <a id="about" href="#">ABOUT US</a>
                        </li>
                        <li>
                            <a id="contact" href="{{asset('contact')}}">CONTACT US</a>
                        </li>
                        @if (Auth::guest())
                        <li>
                            <a id="sign-up" href="{{ url('/auth/register')}}">SIGN UP</a>
                        </li>
                        <li>
                            <a id="sign-in" href="{{ url('/auth/login')}}">SIGN IN</a>
                        </li>
                        @else
                        <li class="dropdown">
                            <a href="#" id="profile" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ asset('profile')}}">Profile</a></li>
                                <li><a href="{{ url('/auth/logout') }}">Logout</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>


        <div id="area-sch" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8">
                <input class="form-control" type="text" id="text-sch">
                <a type="submit" role="button" id="sch-event">
                    <span class="glyphicon glyphicon-search"></span>
                </a>
            </div>
        </div>

        <!-- ======== End Navigation Bar -->
        <!-- ======================= End Header ============================================== -->