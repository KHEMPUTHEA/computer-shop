@extends('pages/template')

@section('content')

    <div class="container mybg-container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="title-body">
                    POPULAR PRODUCTS
                </div>
            </div>
        </div>
        <div class="row myRowC">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php
                        $i = 0;
                        $n = count($carousel);
                    ?>
                    @for($i=0;$i<$n;$i++)
                        @if($i==0)
                            <li data-target="#myCarousel" data-slide-to="$i" class="active"></li>
                        @else
                            <li data-target="#myCarousel" data-slide-to="$i"></li>
                        @endif
                    @endfor
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox" id="my-carousel">
                @foreach($carousel as $CAROUSEL)
                    @if($CAROUSEL->products_id == 90)
                        <div class="item active">
                            <img src="{{$CAROUSEL->path}}">
                            <div class="carousel-caption my-carousel-caption hidden-xs">
                                {{$CAROUSEL->products_name}}
                            </div>
                        </div>
                    @else
                    <div class="item">
                        <img src="{{$CAROUSEL->path}}">
                        <div class="carousel-caption my-carousel-caption hidden-xs">
                            {{$CAROUSEL->products_name}}
                        </div>
                    </div>
                    @endif
                @endforeach
                </div>


                <!-- Controls -->
                <a class="carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a id="my-right" class="carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        {{--==================== Wrapper body of home page =====================--}}
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="title-body">
                    BRANDS
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-body">
                    <a href="{{asset('catalogue')}}" class="img-link">
                        <img src="/images/apple-logo.png" class="img-responsive col-lg-3 col-md-3 col-sm-3 col-xs-3 img-item">
                    </a>
                    <a href="{{asset('catalogue')}}" class="img-link">
                        <img src="/images/dell-logo.png" class="img-responsive col-lg-3 col-md-3 col-sm-3 col-xs-3 img-item">
                    </a>
                    <a href="{{asset('catalogue')}}" class="img-link">
                        <img src="/images/hp-logo.png" class="img-responsive col-lg-3 col-md-3 col-sm-3 col-xs-3 img-item">
                    </a>
                    <a href="{{asset('catalogue')}}" class="img-link">
                        <img src="/images/more-logo.png" class="img-responsive col-lg-3 col-md-3 col-sm-3 col-xs-3 img-item">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="title-body">
                    SHOPPING
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-body">
                    <a href="{{asset('catalogue')}}" class="img-link">
                        <img src="/images/categories-logo.png" class="img-responsive col-lg-3 col-md-3 col-sm-3 col-xs-3 img-item">
                    </a>
                    <a href="{{asset('contact')}}" class="img-link">
                        <img src="/images/delivery-logo.png" class="img-responsive col-lg-3 col-md-3 col-sm-3 col-xs-3 img-item">
                    </a>
                    <a href="{{asset('contact')}}" class="img-link">
                        <img src="/images/contact-logo.png" class="img-responsive col-lg-3 col-md-3 col-sm-3 col-xs-3 img-item">
                    </a>
                    <a href="{{asset('#')}}" class="img-link">
                        <img src="/images/info-logo.png" class="img-responsive col-lg-3 col-md-3 col-sm-3 col-xs-3 img-item">
                    </a>
                </div>
            </div>
        </div>
        {{--<div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="title-body">
                    FEATURED PRODUCTS
                </div>
            </div>
        </div>
        <div class="row myRow">
            @foreach($dell as $DELL)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs12 item-unique">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs12">
                        <a href="#{{$DELL->products_id}}" data-toggle="modal">
                            <img class="img-responsive" src="{{$DELL->path}}">
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs12">
                        <table class="table table-striped item-table">
                            <tr>
                                <td>CPU</td>
                                <td>{{$DELL->cpu}}</td>
                            </tr>
                            <tr>
                                <td>RAM</td>
                                <td>{{$DELL->ram}}</td>
                            </tr>
                            <tr>
                                <td>HDD</td>
                                <td>{{$DELL->hdd}}</td>
                            </tr>
                            <tr>
                                <td>Display</td>
                                <td>{{$DELL->screen}}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs12 item-btn">
                        <a href="#" class="buy-btn">Buy</a>
                        <a href="#{{$DELL->products_id}}" data-toggle="modal" class="detail-btn pull-right">Detail</a>
                    </div>

                    <div class="modal fade font-modal" id="{{$DELL->products_id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="modal-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">{{$DELL->products_name}}</h4>
                                </div>
                                <div class="modal-body col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <img class="img-responsive col-lg-6 col-md-6 col-sm-6 col-xs-12" src="{{$DELL->path}}">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <table class="table table-striped">
                                            <tr>
                                                <td>CPU</td>
                                                <td>{{$DELL->cpu}}</td>
                                            </tr>
                                            <tr>
                                                <td>RAM</td>
                                                <td>{{$DELL->ram}}</td>
                                            </tr>
                                            <tr>
                                                <td>HDD</td>
                                                <td>{{$DELL->hdd}}</td>
                                            </tr>
                                            <tr>
                                                <td>Display</td>
                                                <td>{{$DELL->screen}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="modal-footer col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <a href="#" id="buy-btn-modal">
                                        Buy
                                    </a>
                                    <a href="#" class="line-none">
                                        <img src="/images/add_to_cart.png" id="add">
                                    </a>
                                    <a href="#" class="line-none">
                                        <img src="/images/remove_from_cart.png" id="delete">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>--}}
    </div>
    <script>
        $(document).ready(function(){
            $('#home').addClass('active-page');
        });
    </script>

@endsection
