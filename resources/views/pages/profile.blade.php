@extends('/pages/template')

@section('content')
    {{--<div class="container mybg-container">
        <div class="row myRow">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" id="sbProfile">
                ggg
            </div>
            <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12" id="wkProfile">
                hhh
            </div>
        </div>
    </div>--}}
    <div class="container mybg-container">
        <div class="row myRowP">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                <img src="images/User-blue-icon.png" class="img-responsive img-user">
            </div>
        </div>
        <div class="row myRow">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist" style="font-family: 'Fjalla One', sans-serif">
                        <li role="presentation" class="active">
                            <a href="#search" aria-controls="search" role="tab" data-toggle="tab">Search</a>
                        </li>
                        <li role="presentation">
                            <a href="#upload" aria-controls="upload" role="tab" data-toggle="tab">Upload</a>
                        </li>
                        <li role="presentation">
                            <a href="#update" aria-controls="update" role="tab" data-toggle="tab">Update</a>
                        </li>
                        <li role="presentation">
                            <a href="#delete" aria-controls="delete" role="tab" data-toggle="tab">Delete</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div role="tabpanel" class="rowTap tab-pane active" id="search">
                        <form class="form-horizontal">
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}">

                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label class="control-label">CATEGORIES</label>
                                    <select name="category_name" id="categories" class="form-control">
                                        <option value=""></option>
                                        <option value="Laptop">Laptop</option>
                                        <option value="Desktop">Desktop</option>
                                        <option value="Projector">Projector</option>
                                        <option value="Server">Server</option>
                                        <option value="Monitor">Monitor</option>
                                        <option value="Scanner">Scanner</option>
                                        <option value="Printer">Printer</option>
                                        <option value="Speaker">Speaker</option>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label class="control-label">BRANDS</label>
                                    <select name="brand" id="brand" class="form-control">
                                        <option value=""></option>
                                        <option value="Mac">Mac</option>
                                        <option value="DELL">Dell</option>
                                        <option value="Hp">Hp</option>
                                        <option value="none">All</option>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <button type="submit" formaction="profile" formmethod="post" class="btn btn-danger" style="margin-top: 25px">Search</button>
                                </div>
                            </div>
                        </form>
                        <div class="col-md-2 col-sm-6 col-xs-4">
                            @if((Input::has('category_name')) && !(Input::has('brand')))
                                @foreach($category as $CATEGORY)
                                    <img class="img-responsive" src="{{$CATEGORY->path}}">
                                @endforeach
                            @elseif(!(Input::has('category_name')) && (Input::has('brand')))
                                @foreach($brands as $BRANDS)
                                    <img class="img-responsive" src="{{$BRANDS->path}}">
                                @endforeach
                            @elseif((Input::has('category_name')) && (Input::has('brand')))
                                @foreach($categoryBrand as $CATEGORYBRAND)
                                    <img class="img-responsive" src="{{$CATEGORYBRAND->path}}">
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="upload">
                        <form class="form-horizontal">
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}">

                            <div class="form-group">
                                <div class="col-md-4 col-sm-4 col-xs-12 admin-form">
                                    <label class="control-label">PRODUCT_ID</label>
                                    <input type="text" name="product_id" id="id" class="form-control">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 admin-form">
                                    <label class="control-label">CATEGORIES</label>
                                    <select name="category_name" id="category" class="form-control">
                                        <option value=""></option>
                                        <option value="Laptop">Laptop</option>
                                        <option value="Desktop">Desktop</option>
                                        <option value="Projector">Projector</option>
                                        <option value="Server">Server</option>
                                        <option value="Monitor">Monitor</option>
                                        <option value="Scanner">Scanner</option>
                                        <option value="Printer">Printer</option>
                                        <option value="Speaker">Speaker</option>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 admin-form">
                                    <label class="control-label">BRANDS</label>
                                    <select name="brand" id="brand" class="form-control">
                                        <option value=""></option>
                                        <option value="Mac">Mac</option>
                                        <option value="DELL">Dell</option>
                                        <option value="Hp">Hp</option>
                                        <option value="none">All</option>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 admin-form">
                                    <label class="control-label">PRODUCT_NAME</label>
                                    <input type="text" id="products_name" class="form-control">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 admin-form">
                                    <label class="control-label">PATH</label>
                                    <input type="text" id="path" class="form-control">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 admin-form">
                                    <label class="control-label">RAM</label>
                                    <input type="text" name="ram" id="cpu" class="form-control">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 admin-form">
                                    <label class="control-label">CPU</label>
                                    <input type="text" name="cpu" id="cpu" class="form-control">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 admin-form">
                                    <label class="control-label">HDD</label>
                                    <input type="text" id="hdd" class="form-control">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 admin-form">
                                    <label class="control-label">SCREEN</label>
                                    <input type="text" id="screen" class="form-control">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 admin-form">
                                    <label class="control-label">QUANTITI</label>
                                    <input type="text" id="quantity" class="form-control">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 admin-form">
                                    <label class="control-label">PRICE</label>
                                    <input type="text" id="price" class="form-control">
                                </div>
                            </div>

                            <button type="submit" formaction="profile" formmethod="post" class="btn btn-danger" style="margin-top: 25px">Upload</button>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="update">

                    </div>
                    <div role="tabpanel" class="tab-pane" id="delete">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('#profile').addClass('active-page');
        });
    </script>
@endsection


