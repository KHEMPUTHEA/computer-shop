        <!-- ======================= Start Footer ============================================== -->
        <div id="myfooter">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="title-footer">Information</div>
                        <div>
                            <ul class="list-footer">
                                <li>
                                    <a href="#">About Us</a>
                                </li>
                                <li>
                                    <a href="#">Policy Privacy</a>
                                </li>
                                <li>
                                    <a href="#">Terms and Conditions</a>
                                </li>
                                <li>
                                    <a href="#">Shipping Methods</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    {{--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="title-footer">Brands</div>
                        <div>
                            <ul class="list-footer">
                                <li>
                                    <a href="#">Dell</a>
                                </li>
                                <li>
                                    <a href="#">hp</a>
                                </li>
                                <li>
                                    <a href="#">Mac</a>
                                </li>
                                <li>
                                    <a href="#">Other</a>
                                </li>
                            </ul>
                        </div>
                    </div>--}}
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="title-footer">Customer Service</div>
                        <div>
                            <ul class="list-footer">
                                <li>
                                    <a href="#">Contact Us</a>
                                </li>
                                <li>
                                    <a href="#">YM: cs_</a>
                                </li>
                                <li>
                                    <a href="#">Phone: +23 7777 168</a>
                                </li>
                                <li>
                                    <a href="#">Delivery</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="title-footer">Follow Us</div>
                        <div>
                            <ul class="list-footer">
                                <li>
                                    <a href="#">Google Plus</a>
                                </li>
                                <li>
                                    <a href="#">Facebook</a>
                                </li>
                                <li>
                                    <a href="#">Twitter</a>
                                </li>
                                <li>
                                    <a href="#">RSS Feed</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="copyright">Copyright © 2015 Techno All right reserved</div>
        <!-- ======================= End Footer ============================================== -->

    </body>
</html>