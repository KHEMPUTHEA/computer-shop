@extends('/pages/template')

@section('content')
        <div class="container mybg-container">
            <div class="row">
            {{--==================== Categories =====================--}}
            	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            	    <div class="title-body">
            	        CATEGORIES
            	    </div>
            	    <ul id="list-product" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	        <li>
            	            <a href="#laptop" data-toggle="collapse" aria-expanded="false" aria-controls="laptop">Laptop</a>
            	            <div class="collapse" id="laptop">
            	                <div>
            	                    <ul>
            	                        <li>
            	                            <form>
            	                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
            	                            <input name="category_name" value="Laptop" style="display : none">
            	                            <input name="brand" value="DELL" style="display : none">
            	                            <button type="submit" formaction="products" formmethod="post" class="product-btn">DELL</button>
            	                            </form>
            	                        </li>
            	                        <li>
            	                            <form>
                                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                <input name="category_name" value="Laptop" style="display : none">
                                                <input name="brand" value="Hp" style="display : none">
                                                <button type="submit" formaction="products" formmethod="post" class="product-btn">Hp</button>
                                            </form>
            	                        </li>
            	                        <li>
                                            <form>
                                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                <input name="category_name" value="Laptop" style="display : none">
                                                <input name="brand" value="Mac" style="display : none">
                                                <button type="submit" formaction="products" formmethod="post" class="product-btn">Mac</button>
                                            </form>
                                        </li>
            	                    </ul>
            	                </div>
            	            </div>
            	        </li>
            	        <li class="product">
            	            <a href="#desktop" data-toggle="collapse" aria-expanded="false" aria-controls="desktop">Desktop</a>
                                <div class="collapse" id="desktop">
                                    <div>
                                        <ul>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Desktop" style="display : none">
                                                    <input name="brand" value="DELL" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">DELL</button>
                                                </form>
                                            </li>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Desktop" style="display : none">
                                                    <input name="brand" value="Hp" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">Hp</button>
                                                </form>
                                            </li>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Desktop" style="display : none">
                                                    <input name="brand" value="Mac" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">Mac</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
            	        </li>
            	        <li class="product">
            	            <a href="#projector" data-toggle="collapse" aria-expanded="false" aria-controls="projector">Projector</a>
            	                <div class="collapse" id="projector">
                                    <div>
                                        <ul>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Projector" style="display : none">
                                                    <input name="brand" value="DELL" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">DELL</button>
                                                </form>
                                            </li>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Projector" style="display : none">
                                                    <input name="brand" value="EPSON" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">EPSON</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
            	        </li>
            	        <li class="product">
            	            <a href="#server" data-toggle="collapse" aria-expanded="false" aria-controls="server">Server</a>
                                <div class="collapse" id="server">
                                    <div>
                                        <ul>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Server" style="display : none">
                                                    <input name="brand" value="DELL" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">DELL</button>
                                                </form>
                                            </li>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Server" style="display : none">
                                                    <input name="brand" value="IBM" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">IBM</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
            	        </li>
            	        <li class="product">
            	            <a href="#monitor" data-toggle="collapse" aria-expanded="false" aria-controls="monitor">Monitor</a>
            	                <div class="collapse" id="monitor">
                                    <div>
                                        <ul>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Monitor" style="display : none">
                                                    <input name="brand" value="DELL" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">DELL</button>
                                                </form>
                                            </li>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Monitor" style="display : none">
                                                    <input name="brand" value="Mac" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">Mac</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
            	        </li>
            	        <li class="product">
            	            <a href="#scanner" data-toggle="collapse" aria-expanded="false" aria-controls="scanner">Scanner</a>
            	                <div class="collapse" id="scanner">
                                    <div>
                                        <ul>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Scanner" style="display : none">
                                                    <input name="brand" value="Canon" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">Canon</button>
                                                </form>
                                            </li>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Scanner" style="display : none">
                                                    <input name="brand" value="Hp" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">Hp</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
            	        </li>
            	        <li class="product">
            	            <a href="#printer" data-toggle="collapse" aria-expanded="false" aria-controls="printer">Printer</a>
            	                <div class="collapse" id="printer">
                                    <div>
                                        <ul>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Printer" style="display : none">
                                                    <input name="brand" value="DELL" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">DELL</button>
                                                </form>
                                            </li>
                                            <li>
                                                <form>
                                                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                                    <input name="category_name" value="Printer" style="display : none">
                                                    <input name="brand" value="Canon" style="display : none">
                                                    <button type="submit" formaction="products" formmethod="post" class="product-btn">Canon</button>
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
            	        </li>
            	        <li class="product">
            	            <form>
                                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token()}}"/>
                                <input name="category_name" value = "Speaker" style="display : none">
                                <input name="brand" value = "none" style="display : none">
                                <button type="submit" formaction="products" formmethod="post" id="speaker">Speaker</button>
                            </form>
            	        </li>
            	    </ul>
            	</div>
            {{--==================== Categories =====================--}}
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="title-body">
                        FEATURED PRODUCTS
                    </div>
                    <div class="row myRow">
                        @foreach($dell as $DELL)
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs12 item-unique">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs12">
                                    <a href="#{{$DELL->products_id}}" data-toggle="modal">
                                        <img class="img-responsive" src="{{$DELL->path}}">
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs12">
                                    <table class="table table-striped item-table">
                                        <tr>
                                            <td>CPU</td>
                                            <td>{{$DELL->cpu}}</td>
                                        </tr>
                                        <tr>
                                            <td>RAM</td>
                                            <td>{{$DELL->ram}}</td>
                                        </tr>
                                        <tr>
                                            <td>HDD</td>
                                            <td>{{$DELL->hdd}}</td>
                                        </tr>
                                        <tr>
                                            <td>Display</td>
                                            <td>{{$DELL->screen}}</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs12 item-btn">
                                    <a href="#" class="buy-btn">Buy</a>
                                    <a href="#{{$DELL->products_id}}" data-toggle="modal" class="detail-btn pull-right">Detail</a>
                                </div>

                                <div class="modal fade font-modal" id="{{$DELL->products_id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="modal-header col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">{{$DELL->products_name}}</h4>
                                            </div>
                                            <div class="modal-body col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <img class="img-responsive col-lg-6 col-md-6 col-sm-6 col-xs-12" src="{{$DELL->path}}">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <table class="table table-striped">
                                                        <tr>
                                                            <td>CPU</td>
                                                            <td>{{$DELL->cpu}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>RAM</td>
                                                            <td>{{$DELL->ram}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>HDD</td>
                                                            <td>{{$DELL->hdd}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Display</td>
                                                            <td>{{$DELL->screen}}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <a href="#" id="buy-btn-modal">
                                                    Buy
                                                </a>
                                                <a href="#" class="line-none">
                                                    <img src="/images/add_to_cart.png" id="add">
                                                </a>
                                                <a href="#" class="line-none">
                                                    <img src="/images/remove_from_cart.png" id="delete">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('#catalogue').addClass('active-page');
            });
        </script>
@endsection
