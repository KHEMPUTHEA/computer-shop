@extends('/pages/profile')

@section('subPro')
        <div class="container mybg-container">
            <div class="row">
                @foreach($category as $CATEGORY)
                    <img class="img-responsive" src="{{$CATEGORY->path}}">
                @endforeach
            </div>
        </div>
        <script>
            $(document).ready(function(){
                $('#profile').addClass('active-page');
            });
        </script>
@endsection