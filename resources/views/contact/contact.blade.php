@extends('pages.template')


@section('content')

<div class="container mybg-container">
  @if(Session::has('message'))
    <div class="alert alert-info">
      {{Session::get('message')}}
    </div>
  @else
  
  {{-- <h1>Contact Us </h1> --}}
  {{-- <ul>
    @if ($errors->has())
       <div class="alert alert-danger">
           @foreach ($errors->all() as $error)
               {{ $error }}<br>
           @endforeach
       </div>
    @endif
  </ul> --}}
 {{-- {!! Form::open(array('route' => 'contact_store', 'class' => 'form-horizontal')) !!}

  <div class="form-group">
    {!! Form::submit('<i class="fa fa-pencil"></i> Delete', array('class' => 'btn btn-danger', 'role' => 'button'))!!}
    {!! Form::label('Your Name') !!}
    {!! Form::text('name', null,
      array('required',
      'class'=>'form-control',
      'placeholder'=>'Your name')) !!}
    </div>

    <div class="form-group">
      {!! Form::label('Your E-mail Address') !!}
      {!! Form::text('email', null,
        array('required',
        'class'=>'form-control',
        'placeholder'=>'Your e-mail address')) !!}
      </div>

      <div class="form-group">
        {!! Form::label('Your Message') !!}
        {!! Form::textarea('message', null,
          array('required',
          'class'=>'form-control',
          'placeholder'=>'Your message')) !!}
        </div>

        <div class="form-group">
          {!! Form::submit('Contact Us!',
            array('class'=>'btn btn-primary')) !!}
          </div>
          {!! Form::close() !!} --}}

        <div class="panel-body col-md-offset-2 col-md-8">
          <form class="form-horizontal" method="post" action="contact_store">
            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                   <fieldset>
                       <legend class="text-center header contact-heading">Contact us</legend>

                       <div class="form-group">
                           <span class="col-md-1 text-center"><i class="fa fa-user bigicon"></i></span>
                           <div class="col-md-10">
                               <input id="fname" name="fname" type="text" placeholder="First Name" class="form-control">
                           </div>
                       </div>
                       <div class="form-group">
                           <span class="col-md-1 text-center"><i class="fa fa-user bigicon"></i></span>
                           <div class="col-md-10">
                               <input id="lname" name="lname" type="text" placeholder="Last Name" class="form-control">
                           </div>
                       </div>

                       <div class="form-group">
                           <span class="col-md-1 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
                           <div class="col-md-10">
                               <input id="email" name="email" type="text" placeholder="Email Address" class="form-control">
                           </div>
                       </div>

                       <div class="form-group">
                           <span class="col-md-1 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                           <div class="col-md-10">
                               <input id="phone" name="phone" type="text" placeholder="Phone" class="form-control">
                           </div>
                       </div>

                       <div class="form-group">
                           <span class="col-md-1 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                           <div class="col-md-10">
                               <textarea class="form-control" id="message" name="message" placeholder="Enter your massage for us here. We will get back to you within 2 business days." rows="7"></textarea>
                           </div>
                       </div>

                       <div class="form-group">
                           <div class="col-md-12 text-center">
                               <button type="submit" class="btn-submit">Submit</button>
                           </div>
                       </div>
                   </fieldset>
               </form>
             </div>


  @endif
 </div>

 <script>
     $(document).ready(function(){
         $('#contact').addClass('active-page');
     });
 </script>
@endsection
