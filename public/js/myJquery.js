$(document).ready(function(){
    $('#home').click(function(){
        $('#home').addClass('active-page');
        $('#catalogue').removeClass('active-page');
        $('#about').removeClass('active-page');
        $('#contact').removeClass('active-page');
        $('#sign-up').removeClass('active-page');
        $('#sign-in').removeClass('active-page');
    });
    $('#catalogue').click(function(){
        $('#home').removeClass('active-page');
        $('#catalogue').addClass('active-page');
        $('#about').removeClass('active-page');
        $('#contact').removeClass('active-page');
        $('#sign-up').removeClass('active-page');
        $('#sign-in').removeClass('active-page');
    });
    $('#about').click(function(){
        $('#home').removeClass('active-page');
        $('#catalogue').removeClass('active-page');
        $('#about').addClass('active-page');
        $('#contact').removeClass('active-page');
        $('#sign-up').removeClass('active-page');
        $('#sign-in').removeClass('active-page');
    });
    $('#contact').click(function(){
        $('#home').removeClass('active-page');
        $('#catalogue').removeClass('active-page');
        $('#about').removeClass('active-page');
        $('#contact').addClass('active-page');
        $('#sign-up').removeClass('active-page');
        $('#sign-in').removeClass('active-page');
    });
    $('#sign-up').click(function(){
        $('#home').removeClass('active-page');
        $('#catalogue').removeClass('active-page');
        $('#about').removeClass('active-page');
        $('#contact').removeClass('active-page');
        $('#sign-up').addClass('active-page');
        $('#sign-in').removeClass('active-page');
    });
    $('#sign-in').click(function(){
        $('#home').removeClass('active-page');
        $('#catalogue').removeClass('active-page');
        $('#about').removeClass('active-page');
        $('#contact').removeClass('active-page');
        $('#sign-up').removeClass('active-page');
        $('#sign-in').addClass('active-page');
    });

    $('#my-tog').click(function(){
        $('#area-sch').css({"display": "none"});
    });
    $('#img-sch').click(function(){
        $('#area-sch').toggle();
    });
    $('.close').click(function(){
        $('#area-sch').css({"display": "none"});
    });
    $('#admin-upload').click(function(){
        $('#upload').css({"display": "block"});
        $('#update').css({"display": "none"});
        $('#delete').css({"display": "none"});
        $('#search').css({"display": "none"});
    });
    $('#admin-update').click(function(){
        $('#upload').css({"display": "none"});
        $('#update').css({"display": "block"});
        $('#delete').css({"display": "none"});
        $('#search').css({"display": "none"});
    });
    $('#admin-delete').click(function(){
        $('#upload').css({"display": "none"});
        $('#update').css({"display": "none"});
        $('#delete').css({"display": "block"});
        $('#search').css({"display": "none"});
    });
    $('#admin-search').click(function(){
        $('#upload').css({"display": "none"});
        $('#update').css({"display": "none"});
        $('#delete').css({"display": "none"});
        $('#search').css({"display": "block"});
    });

    $('#sub').click(function(){
        $('#show-pro').css({"display": "block"});
    });
});
