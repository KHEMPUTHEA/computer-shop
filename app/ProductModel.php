<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductModel extends Model {

    public static function getCategory(){

        $category = DB::table('category')->groupBy('category_name')->get();
        return $category;
    }


    public static function getLapDELL(){
        $lap_dell = DB::table('products')->join('category', 'products.products_id', '=', 'category.product_id')->where('category.category_name', '=', 'Laptop')->where('category.brand', '=', 'DELL')->get();
        return $lap_dell;
    }

    public static function getProducts($category_name, $brand){
        $lap_dell = DB::table('products')->join('category', 'products.products_id', '=', 'category.product_id')->where('category.category_name', '=', $category_name)->where('category.brand', '=', $brand)->get();
        return $lap_dell;
    }

    public static function getCarousel(){
        $carousel = DB::table('products')->join('category', 'products.products_id', '=', 'category.product_id')->where('category.category_name', '=', 'carousel')->where('category.brand', '=', 'none')->get();
        return $carousel;
    }
    public static function category($category_name){
        $category = DB::table('products')->join('category', 'products.products_id', '=', 'category.product_id')->where('category.category_name', '=', $category_name)->get();
        return $category;
    }
    public static function brand($brand){
        $brands = DB::table('products')->join('category', 'products.products_id', '=', 'category.product_id')->where('category.brand', '=', $brand)->get();
        return $brands;
    }
    public static function categoryBrand($category_name, $brand){
        $categoryBrand = DB::table('products')->join('category', 'products.products_id', '=', 'category.product_id')->where('category.category_name', '=', $category_name)->where('category.brand', '=', $brand)->get();
        return $categoryBrand;
    }
    public static function upload($newProduct){
        $cate = DB::table('category')->insertGetId(
            array('product_id'=>$newProduct['product_id'],'category_name'=>$newProduct['category_name'], 'brand'=>$newProduct['brand'])
        );
        $Prod = DB::table('products')->insertGetId(
            array('products_name'=>$newProduct['products_name'], 'path'=>$newProduct['path'], 'ram'=>$newProduct['ram'], 'cpu'=>$newProduct['cpu'], 'hdd'=>$newProduct['hdd'], 'screen'=>$newProduct['screen'])
        );
    }

}
