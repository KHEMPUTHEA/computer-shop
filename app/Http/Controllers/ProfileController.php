<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProductModel;
use Illuminate\Support\Facades\App;

use Illuminate\Http\Request;

class ProfileController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	    $this->middleware('auth');
	}

    /**
     * Show the application welcome screen to the user.
     *
     * @param Request $request
     * @return Response
     */
    public function myProfile(Request $request)
    {
        $tmp = $request->except('_token');
        $category = ProductModel::category($tmp['category_name']);
        $brands = ProductModel::brand($tmp['brand']);
        $categoryBrand = ProductModel::categoryBrand($tmp['category_name'], $tmp['brand']);
        ProductModel::upload($tmp);
        return view('/pages.profile')->with(compact('categoryBrand', 'category', 'brands'));
    }
	public function login()
	{
        return view('/pages/profile');
	}
    public function register()
    {
        return view('/pages/profile');
    }

}
