<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function optionProduct(Request $request)
	{
        $tmp = $request->except('_token');
        $category = ProductModel::admin($tmp['category_name']);
        return view('/pages.profile', compact('category'));
	}

}
