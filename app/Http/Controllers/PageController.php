<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ProductModel;
use Illuminate\Support\Facades\App;

use Illuminate\Http\Request;

class PageController extends Controller {

    public function home(){
        //$category = ProductModel::getCategory();
        //$brand = ProductModel::getBrand();
        $carousel = ProductModel::getCarousel();
        $dell = ProductModel::getLapDELL();
        return view('/pages.index', compact('dell'), compact('carousel'));
    }

    public function catalogue(){
        $dell = ProductModel::getLapDELL();
        return view('/pages.catalogue', compact('dell'));
    }

    public function getProducts(Request $request){
        $tmp = $request->except('_token');
        $products = ProductModel::getProducts($tmp['category_name'], $tmp['brand']);
        return view ('/pages.products', compact('products'));
    }


}
