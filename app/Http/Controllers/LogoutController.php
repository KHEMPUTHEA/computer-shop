<?php namespace App\Http\Controllers;
use App\ProductModel;

use App\Product;
class LogoutController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */


    public function __construct()
    {
        $this->middleware('guest');
    }

	public function logout(){
        return view('/pages/login');
	}

	/*public function home()
	{

		  $dell_list = Product::view_list();
          return view('pages.index')->with('dell', $dell_list);
	}*/

    /*public function index()
    {
        return view('/pages/login');
    }*/
}
