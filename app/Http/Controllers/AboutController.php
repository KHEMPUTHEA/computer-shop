<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ContactFormRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class AboutController extends Controller {

		public function create(){
			return view('contact.contact');
		}

		public function store(ContactFormRequest $request){
			\Mail::send('emails.contact',
        array(
            'fname' => $request->get('fname'),
						'lname'=> $request->get('lname'),
            'email' => $request->get('email'),
						'phone' => $request->get('phone'),
            'user_message' => $request->get('message')
        ), function($message)
    {
			$message->from('khmercourse@gmail.com');
     $message->to('khmercourse@gmail.com', 'Admin')->subject('Feedback');
    });

			return \Redirect::to('contact')
      ->with('message', 'Thanks for contacting us!');
		}

}
