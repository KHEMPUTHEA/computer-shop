<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('index', 'PageController@home');
Route::get('catalogue', 'PageController@catalogue');
Route::post('products', 'PageController@getProducts');


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('profile', 'ProfileController@register');
Route::get('profile', 'ProfileController@login');
Route::get('login', 'LogoutController@logout');
Route::post('profile', 'ProfileController@myProfile');
/*Route::post('test', 'ProfileController@test');*/


Route::get('contact', 'AboutController@create');
Route::post('contact_store', 'AboutController@store');


